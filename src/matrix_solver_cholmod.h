/*                                                                                         
**   This program is free software under GNU General Public License.                       
**   (c) Copyright 2015-2015 by Magnus Wangen                                              
**                                                                                         
**   This program is free software; you can redistribute it and/or                         
**   modify it under the terms of the GNU General Public License as                        
**   published by the Free Software Foundation; either version 2 of                        
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
**
**   $Id$
*/

#ifndef _MATRIX_SOLVER_CHOLMOD_H_
#define _MATRIX_SOLVER_CHOLMOD_H_

void abc_demo_cholmod(int argc, char **argv);
void abc_install_cholmod_solver(void);
void abc_solver_cholmod(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);

#endif
