import os

case_params = [
    [0, "x",         "nn=30"],
    [0, "ut-ref",    ""],                      # 400 y
    [0, "ut-200y",   "t_end=6.3072e+09"],      # 200 y
    [0, "ut-400y",   "t_end=1.26144e+10"],     # 400 y
    [0, "ut-800y",   "t_end=2.52288e+10"],     # 800 y
    [0, "ut-kf16",   "perm_frac_closed=1e-16"],    # 400 y
    [0, "ut-kf15",   "perm_frac_closed=1e-15"],    # 400 y
    [0, "ut-kf14",   "perm_frac_closed=1e-14"],    # 400 y
    [0, "ut-str5",   "frac_scale_x=1e+05 frac_scale_y=1e+05 frac_scale_z=1e+05"],
    [1, "ut-str4",   "frac_scale_x=1e+04 frac_scale_y=1e+04 frac_scale_z=1e+04"],
    [0, "ut-s50",    "steps=50"],
    [0, "ut-nn40",   "nn=40"],
    [0, "ut-nn60",   "nn=60"]
];

case_common_params = \
 "open_mode=Chimney base_source=0 t_end=1.26144e+10 " + \
 "nn=50 thickness=1000 size=2000 dome_A0=100 res_h0=100 water_depth=-100 " + \
 "perm_res=1e-12 perm_frac_open=1e-12 perm_frac_closed=1e-17 " + \
 "phi_res=0.3 phi_cap=0.15 phi_dam=0.15 " + \
 " "
for i in range(len(case_params)):
    # Do we have green light for running the case?
    if (case_params[i][0] != 1): continue

    # Make and enter the case directory.
    new_dir = "../chimney-" + case_params[i][1]
    if (not os.path.exists(new_dir)): os.mkdir(new_dir)
    os.chdir(new_dir)
    cdir = os.getcwd()
    print "CURRENT DIR: ", cdir 

    # Make commands:
    cmds = [];
    cmds.append("cp ../src/MakeAnim.sh .")
    cmds.append("bash ../src/MakeOneCase.sh " + case_common_params + case_params[i][2])
    cmds.append("python ../src/make-tex-table.py")
    
    # Execute the commands:
    for cmd in cmds:
        print "RUNNING COMMAND:", cmd
        os.system(cmd)

