/*
**   This program is free software under GNU General Public License.
**   (c) Copyright 2015-2019 by Magnus Wangen
**
**   File: damage3d-ns-chimney.c
**   Title: Code for modelling chimney formation in 3D.
**   Date: Version 2.10, January 2019
**   Author: Magnus Wangen (Magnus.Wangen@gmail.com)
**
**   $Id$
*/

/*
**    \chapter{GNU General Public License}
**
**   This program is free software; you can redistribute it and/or
**   modify it under the terms of the GNU General Public License as
**   published by the Free Software Foundation; either version 2 of
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
*/

/*
**   History:
**
**   Version: 1.00 Jul 2016: First 3D-version based on "damage2d.c".
**   Version: 1.10 Jan 2017: Makes use of network solver.
**   Version: 1.20 Feb 2017: Added lithostatic and anisotropic stress.
**   Version: 1.30 Oct 2017: Added fracture interval.
**   Version: 1.31 Oct 2017: Bond strength depends on bond dir.
**   Version: 1.40 Nov 2017: VTK-output of network.
**   Version: 1.41 Nov 2017: Branch level for each bond in network.
**   Version: 1.42 Nov 2017: Strahler and Shreve stream numbers.
**   Version: 1.43 Nov 2017: Damage perm by Shreve stream number.
**   Version: 1.44 Jan 2018: Damage per row; Checking boundary contact.
**   Version: 1.45 Mar 2018: a, b, fx- and fy are written out unprimed.
**   Version  2.00 Jan 2019: Added a fluid reservoir at the base.
**   Version  2.10 Feb 2019: May run script for making animations.
*/

#include <math.h>
#include <fem.h>
#include <fvm_network_solver.h>

static AbcParams *def_params(void);
static void   init_dir(void);
static void   init_grid(void);
static void   init_pde_pres(void);
static void   coefs_pres_bond(FEMDiffEq *pde, int b, FEMBondCoefs *coefs);
static void   coefs_pres(FEMDiffEq *pde, FEMType *fem, FEMCoefs *coef);
static void   init_arrays(void);
static void   init_bursts(void);
static void   init_rock_properties(void);
static void   init_reservoir_pres(void);
static int    get_res_monitor_elem(void);
static int    get_bond_dir(int b);
static int    get_top_reservoir_elem(void);
static void   fracture_bond_between_elements(int elem1, int elem2);

static void   run_pres_test(void);
static void   pres_build_up(void);
static void   frac_growth_time_step(void);

static void   run_damage_model(void);
static void   damage_propagation(int main_step, double t0, double t1);
static void   check_stream_orders(void);
static void   log_mass_balance(void);
static void   update_fracture_perm(void);
static void   mark_chimneys_reaching_surface(void);
static void   mark_chimney_elems_until_reservoir(int elem0);
static int    get_most_critical_bond(void);
static int    get_weakest_reservoir_bond(void);

static double get_dt(void);
static double get_critical_stress_bond(int bond);
static double get_least_eff_comp_stress(int bond);
static int    is_vertical_top_bond(int bond);
static int    is_top_surface_elem(int elem);
static int    is_close_to_surface_elem(int elem);
static int    is_OK_bond_dir(int bond);
static int    is_open_fractured_bond(int bond);
static int    is_open_chimney_bond(int bond);
static int    is_open_critical_bond(int bond);
static void   break_the_bond(int bond, double t1);
static void   load_current_pres(void);
static void   save_current_pres(void);
static void   restore_current_pres(void);
static void   plot_pres_profile(double t1);

static void   run_IP_test(void);
static void   do_one_time_step(int step, int *bond_start);
static int    get_weakest_bond_from_fracture(void);
static void   try_to_break_elements_from(int elem0, 
                 int burst_id, int *broken_bonds, double fluid_pres);
static int    is_sample_spanning_elem(int elem);

static void   solve_for_fluid_pressure(double t0, double t1);

static void   make_network_solution(double t0, double t1);
static void   make_bond_coefs_ns(FEMDiffEq *pde, int b1, FEMBondCoefs *coefs);
static void   make_elem_coefs_ns(FEMDiffEq *pde, FEMType *fem, FEMCoefs *coef);

static void   make_fast_pres_solution(double t1);
static double get_init_reservoir_pres(void);
static double get_final_reservoir_pres(void);
static double get_first_frac_overpres(void);
static double get_max_overpres(void);
static double get_source_term(int elem);
static double get_reservoir_rate_per_vol(void);
static double get_first_frac_ph(void);
static double get_average_res_pres(void);
static double get_compressed_vol_in_res(void);
static double get_injected_vol(void);
static double get_bulk_reservoir_vol(void);
static double get_total_volume(void);
static double get_cell_volume(void);
static double get_max_dist_from_well(void);
static double get_max_dist_to_frac_elem(void);
static void   print_separator(FILE *out);
static void   print_bond(int bond);
static const char *get_YES_or_NO(int is_yes);
static const char *get_code_name_for_elem(int code);
static void   check_for_intact_bonds(int elem);
static void   fracture_bonds(int step, int bond_start);
static void   add_to_list_of_event_size(int broken_bonds, double t1);
static void   add_to_list_of_fractured_elements(int elem);
static void   add_to_list_of_permeable_elements(int elem);
static void   add_to_list_of_fractured_bonds(int bond, double t1);
static void   add_to_list_of_bursts(int bond);
static void   add_to_list_of_elems_in_bursts(int burst_no, int elem);
static void   print_list(int *list, int size, const char *name, const char *title);
static void   find_all_branch_levels(void);
static void   find_all_branch_levels_from_elem(int elem0, int cur_level);
static void   find_all_fractured_bonds_from_elem(int elem, int *bonds, int *elems2, int *size);
static void   find_longest_branch(int *list2_bond, int *size2, int max_list2_size);
static void   find_shreve_number_for_network(int *smax);
static int    find_shreve_number_from_elem(int elem0, int prev_bond);
static void   find_strahler_number_for_network(int *smax);
static int    find_strahler_number_from_elem(int elem0, int prev_bond);
static int    find_next_bond_and_elem(int prev_bond, int prev_elem, 
                   int *next_bond, int *next_elem);
static void   find_dead_end_bond_and_elem(int max_level, 
                   int *list_bond, int *list_elem, int *size, int max_list_size);
static int    get_downstream_shreve_number(int *sn, int size);
static int    get_downstream_strahler_number(int *sn, int size);
static int    get_dead_end_elem(int b);
static int    get_max_topo_number(void);
static int    comp_reverse_ints(const void *a0, const void *b0);
static void   find_bond_dir_weights(int sum[3], double weight[3]);
static void   show_info(FILE *out);
static int   *create_elem_to_point_map(FILE *out, int size_list, int *list_bonds);
static void   write_animation(int counter, double t1);
static void   write_info(void);
static void   write_boundray_elems(void);
static void   write_results(void);
static void   write_results_for_steps(int step);
static void   write_results_to_file(const char *file_name);
static void   write_to_log_file(int step, int broken_bonds, double fluid_pres);
static void   write_to_damage_log_file(int step, double t1);
static void   write_params_II(AbcOutputStreamII *stream);
static void   write_fracture_network_II(AbcOutputStreamII *stream);
static void   write_fracture_network_vtk1(int s0);
static void   write_fracture_network_vtk2(void);
static void   write_fracture_network_2d_PS(void);
static void   write_fracture_network_python(void);
static void   write_vtk_cell_data(FILE *out, int size);
static void   write_vtk_bond_data_ints(FILE *out, const char *name, int *array);
static void   write_vtk_bond_data_doubles(FILE *out, const char *name, double *array);
static void   write_longest_branch_vtk(void);
static void   write_bond_PS(int bond, int level);
static void   write_all_events_to_file(void);
static void   write_ricther_data(void);
static void   write_elements_in_event(void);
static void   write_damage_per_elem_row(void);
static void   make_burst_size_elem(void);
static double get_random_number(void);
static double get_mean_over_event_elems(int k, double *array_of_elem);
static int    get_number_of_strahler_bonds(int s0);
static int    get_elems_in_event(int i, int *elem_in_event, int max_array);
static double get_min_factor_hh(void);
static double get_unprimed_fac(double fprime);
static double get_primed_fac(double fprime);
static double get_a_param(double sigma_v);
static double get_b_param(void);
static double get_depth_of_bond(int b);
static double get_depth_top_reservoir(double x, double y);
static int    get_chimneys_reaching_surface(int *list);
static int    get_number_of_new_chimney_cells(void);
static int    get_number_of_chimneys_estimate(void);
static void   find_bond_xyz_pos(int b, double *x1, double *y1, double *z1);

#define MODEL_IP            1
#define MODEL_PRES_TEST     2
#define MODEL_DAMAGE        3

#define SOLVER_FULL_GRID    1
#define SOLVER_SCALAR_PRES  2
#define SOLVER_NETWORK      3

#define MAX_BINS         1024
#define MAX_EVENTS      32768
#define MAX_LIST_SIZE    1024

#define OPEN_CHIMNEY_BOND   1
#define OPEN_CRITICAL_BOND  2

static AbcParams *params1 = NULL;
static FEMGrid *grid1 = NULL;
static FEMDiffEq *pde_pres = NULL;
static FEMNetworkSolver *ns = NULL;
static FVMMassBalanceDomain massbal;

static char
     dir_name[ABC_MAX_WORD] = ".",
     case_name[ABC_MAX_WORD] = "x1";

static int
     nl_nodes = 26,
     nv_nodes = 1,
     n_xdomes = 0,
     n_ydomes = 0,
     n_bins = 64,
     n_events = 0,
     n_bursts = 0,
     n_frac_elems = 0,
     n_frac_bonds = 0,
     n_perm_elems = 0,
     n_main_steps = 10,
     n_init_frac_elems = 1,
     n_substep_output = 10,
     is_verbose = TRUE,
     is_debugging = TRUE,
     is_sample_spanning = FALSE,
     is_isotropic_frac_scale = TRUE,
     is_just_giving_info = FALSE,
     is_loopless = TRUE,
     is_base_source = FALSE,
     is_fxy_by_eff_stress = FALSE,
     is_network_analysis = FALSE,
     is_dynamic_frac_perm = FALSE,
     is_making_animation = FALSE,
     has_lithostatic = FALSE,
     output_step_size = 0,
     open_bond_mode = -1,
     model_type = -1,
     solver_type = -1,
     top_res_elem = -1,
     first_frac_elem = -1,
     res_monitor_elem = -1,
     max_frac_elems = 999999,
     max_topo_number = -1,
     max_shreve_number = -1,
     max_strahler_number = -1,
     seed = -123;

static double
     t_end = 1.0,
     size_lateral = 25.999,
     size_vertical = -1.0,
     res_h0 = 0.0,
     dome_A0 = 0.0,
     compres0 = 1.0e-8,
     visc0 = 1.0e-3,
     phi0 = 0.2,
     phi_res = 0.0,
     phi_cap = 0.0,
     phi_dam = 0.0,
     pfac1 = 1.0,
     pfac2 = 1.0,
     perm_res = 1.0e-16,
     perm_rock = 1.0e-16,
     perm_frac_open = 1.0e-12,
     perm_frac_closed = 1.0e-12,
     dens_fluid = 1000.0,
     dens_bulk = 2200.0,
     depth_top_surf = 0.0,
     pres_first_frac = 0.0,
     sigma_h = 20.0e+6,
     gravity = -9.8,
     source_res = 0.0,
     source_base = 0.0,
     water_depth = 0.0,
     factor_hh[3] = {1.0, 1.0, 1.0},
     factor_min_hh[3] = {1.0, 1.0, 1.0},
     frac_scale = 1.0e+6,
     frac_scale_dir[3] = {1.0e+6, 1.0e+6, 1.0e+6};

static int 
     *dir_bond = NULL,
     *topo_bond = NULL,
     *shreve_bond = NULL,
     *strahler_bond = NULL,
     *first_elem_in_burst = NULL,
     *list_of_event_size = NULL,
     *list_of_frac_elems = NULL,
     *list_of_frac_bonds = NULL,
     *list_of_perm_elems = NULL,
     *list_of_burst_size = NULL,
     *is_fractured_elem = NULL,
     *is_fractured_bond = NULL,
     *is_reservoir_elem = NULL,
     *is_permeable_elem = NULL,
     *is_domain_elem = NULL,
     *is_surface_chimney_elem = NULL,
     *next_elem_in_burst = NULL,
     *burst_elem = NULL,
     *burst_size_elem = NULL,
     *events_elem = NULL,
     *buffer_of_elem = NULL;

static double
     *perm_bond = NULL,
     *phi_elem = NULL,
     *pres_elem = NULL,
     *pres_saved_elem = NULL,
     *frac_limit_bond = NULL,
     *darcy_flow_bond = NULL,
     *event_time1 = NULL,
     *event_time2 = NULL,
     *time_bond = NULL,
     *event_time1_elem = NULL,
     *event_time2_elem = NULL,
     *hydrostatic_elem = NULL,
     *lithostatic_elem = NULL,
     *eff_lithostatic_elem = NULL;


int main(
     int argc,
     char **argv)
{
     printf("=============================================\n");
     printf("This is DAMAGE-3D version 2.10, from Feb 2019\n");
     printf("=============================================\n");
     printf("(Solver opstions: Full, Scalar, Network)\n");
#ifdef ABC_CHOLMOD
     abc_install_cholmod_solver();
#else
     printf("(info: Cholmod is not installed!)\n");
#endif

     params1 = def_params();
     abc_read_all_params_by_args(params1, argc, argv);
     abc_print_all_params_to_file(params1, case_name, "-input.txt");
     abc_print_all_params_to_a2_file(params1, case_name, "-input.a2");
     abc_print_all_params_to_tex_file(params1, case_name, "-params.tex");

     init_dir();
     init_grid();
     init_pde_pres();
     init_arrays();
     init_rock_properties();
     init_bursts();

     if (not is_just_giving_info)
     {
          switch (model_type)
          {
               case MODEL_IP:        run_IP_test();       break;
               case MODEL_DAMAGE:    run_damage_model();  break;
               case MODEL_PRES_TEST: run_pres_test();     break;
               default: ABC_ERROR_EXIT("Illegal model!");
          }
     }

     show_info(stdout);

     fvm_delete_network_solver(&ns);
     fem_delete_pde(&pde_pres);
     fem_delete_grid(&grid1);
     abc_delete_params(&params1);
     ABC_DELETE_LIB();

     return 1;
}


static AbcParams *def_params(
     void)
{
     int GR1 = 1;
     AbcParams *params = abc_new_params(1024);

     abc_use_params_input_block(params, TRUE);
     abc_def_param_group(params, "Parameters:", GR1);

     abc_def_string_param(params, dir_name, "dir_name",
          "The name of a directory for results", ".", ABC_MAX_WORD, GR1);
     abc_def_string_param(params, case_name, "case_name",
          "A common name (case name) for all output files", "x1", ABC_MAX_WORD, GR1);
     abc_def_int_param(params, &n_bins, "bins", "-",
          "Number of magnitude bins", 64, 4, MAX_BINS, GR1);
     abc_def_int_param(params, &n_main_steps, "steps", "-",
          "Number of time-steps", 10, 1, 1024, GR1);
     abc_def_int_param(params, &output_step_size, "output_step_size", "-",
          "Output step size (interval)", 0, 0, 99999, GR1);
     abc_def_int_param(params, &n_substep_output, "output_substep_size", "-",
          "Output substep size (interval)", 10, 0, 99999, GR1);
     abc_def_int_param(params, &nl_nodes, "nn", "-",
          "Number of nodes in x- and y-direction", 50, 4, 1024, GR1);
     abc_def_int_param(params, &nv_nodes, "nv", "-",
          "Number of nodes in z-direction", 1, 1, 1024, GR1);
     abc_def_int_param(params, &n_init_frac_elems, "init_frac_elems", "-",
          "Number of elements in initial fracture", 1, 1, 1024, GR1);
     abc_def_double_param(params, &size_lateral, "size", "m",
          "System size", 11.999, 1.0e-6, 1.0e+6, GR1);
     abc_def_double_param(params, &size_vertical, "thickness", "m",
          "System thickness", 0.001, 1.0e-6, 1.0e+6, GR1);
     abc_def_double_param(params, &dens_bulk, "dens_bulk", "kg/m3",
          "Bulk density", 2200.0, 0.0, 9999.0, GR1);
     abc_def_double_param(params, &water_depth, "water_depth", "m",
          "Water depth", 0.0, -10.0e+3, 0.0, GR1);
     abc_def_double_param(params, &depth_top_surf, "depth_top_surf", "m",
          "Depth to top of domain", 0.0, -10.0e+3, 0.0, GR1);
     abc_def_double_param(params, &sigma_h, "sigmah", "Pa",
          "Least compressive stress", 0.0, 0.0, 1.0e+9, GR1);
     abc_def_double_param(params, &compres0, "compres", "1/Pa",
          "Compressibility", 1.0e-8, 1.0e-12, 1.0e-6, GR1);
     abc_def_double_param(params, &visc0, "visc", "Pa s",
          "Viscosity", 1.0e-3, 1.0e-6, 1.0, GR1);
     abc_def_double_param(params, &perm_res, "perm_res", "m2",
          "Rock permeability", 1.0e-15, 1.0e-21, 1.0e-10, GR1);
     abc_def_double_param(params, &perm_rock, "perm_rock", "m2",
          "Rock permeability", 1.0e-15, 1.0e-23, 1.0e-10, GR1);
     abc_def_double_param(params, &perm_frac_open, "perm_frac_open", "m2",
          "Open fracture permeability", 1.0e-12, 1.0e-21, 1.0e-6, GR1);
     abc_def_double_param(params, &perm_frac_closed, "perm_frac_closed", "m2",
          "Closed fracture permeability", 1.0e-15, 1.0e-21, 1.0e-6, GR1);
     abc_def_double_param(params, &phi0, "phi", "-",
          "Porosity", 0.15, 0.001, 0.99, GR1);
     abc_def_double_param(params, &phi_res, "phi_res", "-",
          "Reservoir porosity", 0.0, 0.0, 0.99, GR1);
     abc_def_double_param(params, &phi_cap, "phi_cap", "-",
          "Caprock porosity", 0.0, 0.0, 0.99, GR1);
     abc_def_double_param(params, &phi_dam, "phi_dam", "-",
          "Damaged rock porosity", 0.0, 0.0, 0.99, GR1);
     abc_def_double_param(params, &pfac1, "pfac1", "-",
          "Initial reservoir press of critical stress", 1.0, 0.0, 1000, GR1);
     abc_def_double_param(params, &pfac2, "pfac2", "-",
          "Final reservoir press of critical stress", 1.0, 0.0, 1000, GR1);
     abc_def_double_param(params, &t_end, "t_end", "s",
          "Time for end-of-simulation", 24.0 * 3600.0, 0.000, 1.0e+15, GR1);
     abc_def_double_param(params, &factor_hh[0], "factor_x", "-",
          "Compressive stress in x-direction", 1.0, 0.0, 2.5, GR1);
     abc_def_double_param(params, &factor_hh[1], "factor_y", "-",
          "Compressive stress in y-direction", 1.0, 0.0, 2.5, GR1);
     abc_def_double_param(params, &factor_hh[2], "factor_z", "-",
          "Compressive stress in z-direction", 1.0, 0.0, 2.5, GR1);
     abc_def_double_param(params, &frac_scale, "frac_scale", "Pa",
          "Fracture strength scale", 1.0, 0.0, 1.0e+12, GR1);
     abc_def_double_param(params, &frac_scale_dir[0], "frac_scale_x", "Pa",
          "Fracture strength scale", 1.0, 0.0, 1.0e+12, GR1);
     abc_def_double_param(params, &frac_scale_dir[1], "frac_scale_y", "Pa",
          "Fracture strength scale", 1.0, 0.0, 1.0e+12, GR1);
     abc_def_double_param(params, &frac_scale_dir[2], "frac_scale_z", "Pa",
          "Fracture strength scale", 1.0, 0.0, 1.0e+12, GR1);
     abc_def_double_param(params, &dome_A0, "dome_A0", "m",
          "Reservoir dome amplitude", 0.0, 0.0, 1.0e+4, GR1);
     abc_def_double_param(params, &res_h0, "res_h0", "m",
          "Reservoir base thickness", 0.0, 0.0, 1.0e+4, GR1);
     abc_def_int_param(params, &n_xdomes, "nxdomes", "-",
          "Number of domes in x-direction", 1, 0, 25, GR1);
     abc_def_int_param(params, &n_ydomes, "nydomes", "-",
          "Number of domes in y-direction", 1, 0, 25, GR1);
     abc_def_int_param(params, &max_frac_elems, "max_frac_elems", "-",
          "Max allowed fractured elements", 9999999, 0, 9999999, GR1);
     abc_def_int_param(params, &seed, "seed", "-",
          "Seed in random number generator", -123, -99999, 99999, GR1);
     abc_def_boolean_param(params, &has_lithostatic, "lithostatic",
          "Enables lithostatic stress", FALSE, GR1);
     abc_def_boolean_param(params, &is_isotropic_frac_scale, "iso_frac_strength",
          "Isotropic fracture strength", TRUE, GR1);
     abc_def_boolean_param(params, &is_loopless, "loopless",
          "Enable/disable the loopless feature", TRUE, GR1);
     abc_def_boolean_param(params, &is_base_source, "base_source",
          "Enable/disable a base model source term", FALSE, GR1);
     abc_def_boolean_param(params, &is_just_giving_info, "info",
          "Will just output case info", FALSE, GR1);
     abc_def_boolean_param(params, &is_verbose, "verbose",
          "Enable/disable verbose mode", TRUE, GR1);
     abc_def_boolean_param(params, &is_debugging, "debug",
          "Enable/disable debugging mode", TRUE, GR1);
     abc_def_boolean_param(params, &is_fxy_by_eff_stress, "fxy_eff_stress",
          "Enable/disable old version of eff stress", TRUE, GR1);
     abc_def_boolean_param(params, &is_dynamic_frac_perm, "is_dynamic_frac_perm",
          "Enable/disable dynamic fracture permeability", FALSE, GR1);
     abc_def_boolean_param(params, &is_making_animation, "is_making_anim",
          "Enable/disable calls on MakeAmin.sh", FALSE, GR1);

     abc_def_label_param2(params, &model_type, "Fracture test type",
                           "model", "IP",         MODEL_IP, GR1);
     abc_add_label(params, "model", "PresTest",   MODEL_PRES_TEST);
     abc_add_label(params, "model", "Damage",     MODEL_DAMAGE);

     abc_def_label_param2(params, &solver_type, "Solver type",
                           "solver", "Network",  SOLVER_NETWORK, GR1);
     abc_add_label(params, "solver", "Scalar",   SOLVER_SCALAR_PRES);
     abc_add_label(params, "solver", "Full",     SOLVER_FULL_GRID);

abc_def_label_param2(params, &open_bond_mode, "Open fractured bond mode",
                           "open_mode", "Critical",  OPEN_CRITICAL_BOND, GR1);
     abc_add_label(params, "open_mode", "Chimney",   OPEN_CHIMNEY_BOND);

     return params;
}


static void init_dir(
     void)
{
     char string[ABC_MAX_WORD];

     abc_make_dir(dir_name);
     sprintf(string, "%s/%s", dir_name, case_name);
     abc_fix_dir_separator(string);
     strcpy(case_name, string);
     printf("(case results have filename beginning with: %s)\n", case_name);
}


static void init_grid(
     void)
{
     if (nv_nodes < 2)
     {
          printf("(number of vertical nodes: %d is set equal to lateral nodes: %d)\n",
               nv_nodes, nl_nodes);

          nv_nodes = nl_nodes;
     }

     if (size_vertical < 1.0)
     {
          printf("(thickness: %g [m] is set equal to lateral size: %g [m])\n", 
               size_vertical, size_lateral);

          size_vertical = size_lateral;
     }

     /* Odd numbers assure that there is a center element. */

     /***
     if (nl_nodes % 2 == 1)
          ABC_ERROR_EXIT("nx- and ny-nodes must be odd numbers!");
     ***/

     /* Make centered unit grid. */

     grid1 = fem_new_linear_hexa_grid(nl_nodes, nl_nodes, nv_nodes);
     fem_translate_grid(grid1, -0.5, -0.5, -1.0);
     fem_scale_grid(grid1, size_lateral, size_lateral, size_vertical);
     fem_translate_grid(grid1, 0.0, 0.0, depth_top_surf);
     fem_make_volume_and_center_coords(grid1);
     fvm_add_bonds(grid1);

     if (is_verbose)
     {
          printf("(depth to top of system:  %.3f [m])\n", depth_top_surf); 
          printf("(depth to base of system: %.3f [m])\n", depth_top_surf - size_vertical); 

          printf("(system size: %g x %g x %g [m])\n", 
               size_lateral, size_lateral, size_vertical);

          printf("(grid: %d x %d x %d elements)\n", 
               grid1->n_x_elems, grid1->n_y_elems, grid1->n_z_elems);
     }
}


static void init_pde_pres(
     void)
{
     int BC1 = FEMBC1stKind;
     const char *options = "-dummy -matSparse -solverCG -precILU -itEpsilon 1e-15";
     /***
     const char *options = "-dummy -matSparse -solverCholmod";
     ***/

     pde_pres = fvm_create_bond_pde_by_string_args(grid1,
          coefs_pres, coefs_pres_bond, options);

     fem_set_pde_name(pde_pres, "Pressure equation (FVM)");
     pde_pres->is_verbose =TRUE;

     /***
     pde_pres->solve_lin_eq_system = solve_Axb_pres_one_fact;
     ***/

     /*
     ** The top surface is hydrostatic and 
     ** the other boundaries are closed for fluid flow.
     */

     fem_add_boundary_condition(pde_pres, BC1, FEMBoundaryTop, 0, fem_zero_value);

     /***
     fem_add_boundary_condition(pde_pres, BC1, FEMBoundaryRight, 0, fem_zero_value);
     fem_add_boundary_condition(pde_pres, BC1, FEMBoundaryLeft, 0, fem_zero_value);
     fem_add_boundary_condition(pde_pres, BC1, FEMBoundaryBot, 0, fem_zero_value);
     ***/
}


static void coefs_pres_bond(
     FEMDiffEq *pde,
     int b,
     FEMBondCoefs *coefs)
{
     coefs->diffusivity = perm_bond[b] / visc0;
     coefs->have_velocity_term = FALSE;
     coefs->velocity = 0.0;

     ABC_UNUSED_PARAMETER(pde);
}


static void coefs_pres(
     FEMDiffEq *pde,
     FEMType *fem,
     FEMCoefs *coef)
{
     int elem = fem->element;
     /**** YYY
     double rate = get_reservoir_rate_per_vol();
     ****/
     double rate = get_source_term(elem);

     /* Part 1: Storage */

     coef->have_storage_term = TRUE;
     coef->storage = phi_elem[elem] * compres0;

     /* Part 2: Diffusivity */

     coef->have_velocity_term = FALSE;

     coef->diffusivity[0][0] = 1.0;
     coef->diffusivity[1][1] = 1.0;
     coef->diffusivity[2][2] = 1.0;

     /* Part 3: Source */

     coef->have_source_term = FALSE;
     coef->source = 0.0;

     if (not is_reservoir_elem[elem]) return;

     coef->have_source_term = TRUE;
     coef->source = rate;

     ABC_UNUSED_PARAMETER(pde);
}


static void init_arrays(
     void)
{
     int n_elems = grid1->n_elems;
     int n_bonds = grid1->n_bonds;

     if (n_bonds < 1)
          ABC_ERROR_EXIT("Bonds are missing in the grid!");

     dir_bond = abc_alloc_ints_once(n_bonds);
     topo_bond = abc_alloc_ints_once(n_bonds);
     shreve_bond = abc_alloc_ints_once(n_bonds);
     strahler_bond = abc_alloc_ints_once(n_bonds);
     list_of_event_size = abc_alloc_ints_once(MAX_EVENTS);
     list_of_frac_elems = abc_alloc_ints_once(n_elems);
     list_of_frac_bonds = abc_alloc_ints_once(n_bonds);
     list_of_perm_elems = abc_alloc_ints_once(n_elems);
     list_of_burst_size = abc_alloc_ints_once(n_elems);
     is_fractured_elem = abc_alloc_ints_once(n_elems);
     is_fractured_bond = abc_alloc_ints_once(n_bonds);
     is_reservoir_elem = abc_alloc_ints_once(n_elems);
     is_permeable_elem = abc_alloc_ints_once(n_elems);
     is_domain_elem = abc_alloc_ints_once(n_elems);
     is_surface_chimney_elem = abc_alloc_ints_once(n_elems);
     phi_elem = abc_alloc_doubles_once(n_elems);
     pres_elem = abc_alloc_doubles_once(n_elems);
     pres_saved_elem = abc_alloc_doubles_once(n_elems);
     frac_limit_bond = abc_alloc_doubles_once(n_bonds);
     darcy_flow_bond = abc_alloc_doubles_once(n_bonds);
     burst_elem = abc_alloc_ints_once(n_elems);
     first_elem_in_burst = abc_alloc_ints_once(MAX_EVENTS);
     next_elem_in_burst = abc_alloc_ints_once(n_elems);
     perm_bond = abc_alloc_doubles_once(n_bonds);
     time_bond = abc_alloc_doubles_once(n_bonds);
     event_time1 = abc_alloc_doubles_once(MAX_EVENTS);
     event_time2 = abc_alloc_doubles_once(MAX_EVENTS);
     burst_size_elem = abc_alloc_ints_once(n_elems);
     events_elem = abc_alloc_ints_once(n_elems);
     buffer_of_elem = abc_alloc_ints_once(n_elems);
     hydrostatic_elem = abc_alloc_doubles_once(n_elems);
     lithostatic_elem = abc_alloc_doubles_once(n_elems);
     eff_lithostatic_elem = abc_alloc_doubles_once(n_elems);
     event_time1_elem = abc_alloc_doubles_once(n_elems);
     event_time2_elem = abc_alloc_doubles_once(n_elems);

     ABC_INIT_ARRAY(burst_elem, n_elems, -1);
     ABC_INIT_ARRAY(first_elem_in_burst, MAX_EVENTS, -1);
     ABC_INIT_ARRAY(next_elem_in_burst, n_elems, -1);
}


static void init_bursts(
     void)
{
     n_bursts = 0;

     ABC_INIT_ARRAY(burst_elem, grid1->n_elems, -1);
     ABC_INIT_ARRAY(list_of_burst_size, grid1->n_elems, 0);
}


static void init_rock_properties(
     void)
{
     int b, i, elem, elem1, elem2, OK1, OK2;
     int n_h_elems = fem_get_n_h_elems(grid1);
     double *depth_elem = grid1->zcoord_elem;
     double pres_wd = dens_fluid * gravity * water_depth;
     double factor, x1, y1, z1, zb, zr, r1;

     if (phi_res < 0.001) phi_res = phi0;
     if (phi_cap < 0.001) phi_cap = phi0;
     if (phi_dam < 0.001) phi_dam = phi0;

     printf("(water depth: %g [m], water load: %g [Pa])\n", 
          water_depth, pres_wd);

     if (pres_wd < -0.001) 
          ABC_ERROR_EXIT("Negative water load!");

     factor_min_hh[0] = ABC_MIN(factor_hh[1], factor_hh[2]);
     factor_min_hh[1] = ABC_MIN(factor_hh[0], factor_hh[2]);
     factor_min_hh[2] = ABC_MIN(factor_hh[0], factor_hh[1]);
 
     if (is_isotropic_frac_scale)
          for (i = 0; i < 3; i++)
               frac_scale_dir[i] = frac_scale;

     for (b = 0; b < grid1->n_bonds; b++)
     {
          is_fractured_bond[b] = FALSE;
          perm_bond[b] = perm_rock;
          dir_bond[b] = get_bond_dir(b);
          r1 = get_random_number();
          frac_limit_bond[b] = frac_scale_dir[dir_bond[b]] * r1;
     }

     for (elem = 0; elem < grid1->n_elems; elem++)
     {
          phi_elem[elem] = phi_cap;
          pres_elem[elem] = 0.0;
          is_fractured_elem[elem] = FALSE;
          is_reservoir_elem[elem] = FALSE;
          is_permeable_elem[elem] = FALSE;
          hydrostatic_elem[elem] = pres_wd + (depth_elem[elem] * gravity * dens_fluid);
          lithostatic_elem[elem] = pres_wd + (depth_elem[elem] * gravity * dens_bulk);
          eff_lithostatic_elem[elem] = lithostatic_elem[elem] - hydrostatic_elem[elem];
     }

     /* Initialize a reservoir layer at the base of the model. */

     for (b = 0; b < grid1->n_bonds; b++)
     {    
          find_bond_xyz_pos(b, &x1, &y1, &z1);

          zr = get_depth_top_reservoir(x1, y1);
          zb = get_depth_of_bond(b);

          if (zb > zr) continue;

          elem1 = grid1->bond_elem1[b];
          elem2 = grid1->bond_elem2[b];

          if (elem1 < 0 and elem2 < 0)
                ABC_ERROR_EXIT("[init_rock_properties] Illegal bond!");

          if (elem1 >= 0) is_reservoir_elem[elem1] = TRUE;
          if (elem2 >= 0) is_reservoir_elem[elem2] = TRUE;

          if (elem1 >= 0 and not is_permeable_elem[elem1])
               add_to_list_of_permeable_elements(elem1);

          if (elem2 >= 0 and not is_permeable_elem[elem2])
               add_to_list_of_permeable_elements(elem2);

          perm_bond[b] = perm_res;
          phi_elem[elem1] = phi_res;
          phi_elem[elem2] = phi_res;
     }

     top_res_elem = get_top_reservoir_elem();
     res_monitor_elem = get_res_monitor_elem();
     first_frac_elem = get_weakest_reservoir_bond();
     pres_first_frac = get_first_frac_overpres();

     /* 
     ** The network solver uses pres=0 as BC in the other
     ** end of bonds that connects permeable elements
     ** non-permeable elements.
     ** To assure that the surface is hydrostatic,
     ** the bonds that connects to the surface elements are
     ** permeable. They have a high permeability (1 D = 1e-12 m2).
     ** Is this necessary?
     */

     for (b = 0; b < grid1->n_bonds; b++)
     {
          elem1 = grid1->bond_elem1[b];
          elem2 = grid1->bond_elem2[b];

          OK1 = (grid1->elem_info[elem1] & FEMBoundaryTop);
          OK2 = (grid1->elem_info[elem2] & FEMBoundaryTop);

          if (OK1 or OK2) perm_bond[b] = 1.0e-12;
     }

     /* Compute source terms for the pressure equation. */

     factor = ((double) n_perm_elems) / ((double) n_h_elems);
     source_res = get_reservoir_rate_per_vol();
     source_base = factor * source_res;
}


static void init_reservoir_pres(
     void)
{
     int n, elem;
     double pres = get_init_reservoir_pres();
     double *perm0_elem = pde_pres->solution0->value;
     double *perm1_elem = pde_pres->solution1->value;

     if (is_verbose)
          printf("(init reservoir pressure is: %g [Pa])\n", pres);

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          if (not is_reservoir_elem[elem]) continue;
          perm0_elem[elem] = pres;
          perm1_elem[elem] = pres;
     }
}


static int get_res_monitor_elem(
     void)
{
     int nx = grid1->n_x_elems;
     int ny = grid1->n_y_elems;
     int nn = (ny / 2) * nx + (nx / 2);
     return nn;
}


static int get_bond_dir(
     int b)
{
     if (fvm_is_bond_in_direction(grid1, b, 0)) return 0;
     if (fvm_is_bond_in_direction(grid1, b, 1)) return 1;
     if (fvm_is_bond_in_direction(grid1, b, 2)) return 2;

     printf("Bond %d does not have direction 1, 2 or 3!\n", b);
     ABC_ERROR_EXIT("Cannot continue!");

     return -1;
}


static int get_top_reservoir_elem(
     void)
{
     int elem, elem_max = 0;
     double *zcoord_elem = grid1->zcoord_elem;
     double zpos_max = zcoord_elem[0];

     for (elem = 0; elem < grid1->n_elems; elem++)
     {
          if (not is_reservoir_elem[elem]) continue;

          if (zcoord_elem[elem] > zpos_max)
          {
               zpos_max = zcoord_elem[elem];
               elem_max = elem;
          }
     }

     return elem_max;
}


static void fracture_bond_between_elements(
     int elem1,
     int elem2)
{
     int i, bond, e1, e2, size, bond_array[8];

     fvm_get_bonds_for_elem(grid1, elem1, bond_array, &size);

     for (i = 0; i < size; i++)
     {
          bond = bond_array[i];
          e1 = grid1->bond_elem1[bond];
          e2 = grid1->bond_elem2[bond];

          if ((e1 == elem1 and e2 == elem2) or
              (e1 == elem2 and e2 == elem1))
          {
               if (not is_fractured_elem[elem1])
                    add_to_list_of_fractured_elements(elem1);

               if (not is_fractured_elem[elem2])
                    add_to_list_of_fractured_elements(elem2);

               if (is_fractured_bond[bond])
                    ABC_ERROR_EXIT("[fracture_bond_between_elements] Broken?");

               add_to_list_of_fractured_bonds(bond, 0.0);
               printf("(bond: %d between elements: %d and %d is broken)\n", bond, e1, e2);
               return;
          }
     }

     ABC_ERROR_EXIT("Can't find a bond between two elements!");
}

/*
**   =======================
**   The pressure test model
**   =======================
*/

static void run_pres_test(
     void)
{
     pres_build_up();
     frac_growth_time_step();
     write_results();
}


static void pres_build_up(
     void)
{
     int i;
     double dt = t_end / (n_main_steps - 1);

     for (i = 1; i < n_main_steps; i++)
     {
          double t0 = (i - 1) * dt;
          double t1 = i * dt;

          fvm_do_time_step(pde_pres, t0, t1);
          load_current_pres();
          plot_pres_profile(t1);

          printf("(time stepping: %g to %g, pres at center: %g)\n", 
               t0, t1, pres_elem[top_res_elem]);
     }
}


static void frac_growth_time_step(
     void)
{
     double dt = t_end / (n_main_steps - 1);
     double t0 = t_end;
     double t1 = t0 + dt;
     int n_h_elems = fem_get_n_h_elems(grid1);
     int i, elem1, elem2;

     printf("(is starting fracture growth pressure test)\n");

     save_current_pres();

     for (i = 0; i < n_init_frac_elems; i++)
     {
          elem1 = top_res_elem + i * n_h_elems;
          elem2 = top_res_elem + (i + 1) * n_h_elems;
          fracture_bond_between_elements(elem1, elem2);

          restore_current_pres();
          fvm_do_time_step(pde_pres, t0, t1);
          load_current_pres();
          plot_pres_profile(t1);

          printf("(well pressure: %g)\n", pres_elem[top_res_elem]);
     }
}

/*
**   ================
**   The damage model
**   ================
*/

static void run_damage_model(
     void)
{
     int i;
     double dt = t_end / (n_main_steps - 1);

     if (is_verbose)
          printf("(is running the DAMAGE model)\n");

     fvm_init_mass_balance_for_domain(&massbal);
     fvm_begin_mass_balance_for_domain(&massbal, pde_pres,
          "mass-bal", "x1-mass-bal.csv", is_domain_elem);

     for (i = 1; i < n_main_steps; i++)
     {
          double t0 = (i - 1) * dt;
          double t1 = i * dt;

          print_separator(stdout);
          printf("(beginning step %d from: %g to: %g)\n", i, t0, t1);
          fflush(stdout);

          /***
          solve_for_fluid_pressure(t0, t1);
          ***/
          damage_propagation(i, t0, t1);

          write_to_damage_log_file(i, t1);
          write_results_for_steps(i);
          write_animation(i, t1);

          printf("(base reservoir pres: %g [Pa])\n", pres_elem[res_monitor_elem]);
          printf("(chimneys reaching surface: %d)\n", get_chimneys_reaching_surface(NULL));
          fflush(stdout);
     }

     fvm_end_mass_balance_for_domain(&massbal);

     write_info();
     write_boundray_elems();
     write_all_events_to_file();
     write_ricther_data();
     write_elements_in_event();
     write_fracture_network_vtk1(-1);
     write_fracture_network_vtk2();
     write_fracture_network_2d_PS();
     write_fracture_network_python();
     write_longest_branch_vtk();
     write_damage_per_elem_row();
     write_results();

     for (i = 1; i <= max_strahler_number; i++)
          write_fracture_network_vtk1(i);
}


static void damage_propagation(
     int step,
     double t0,
     double t1)
{
     int i, bond;
     int n0 = get_res_monitor_elem();
     double ph = get_first_frac_ph();
     static int counter = 1;

     init_bursts();

     update_fracture_perm();
     solve_for_fluid_pressure(t0, t1);
     save_current_pres();

     while ((bond = get_most_critical_bond()) >= 0)
     {
          break_the_bond(bond, t1);

          printf("(MAIN STEP: %d, substep: %d, ", step, counter);

          if (first_frac_elem >= 0)
          {
               printf("Pf-base-res: %.3f [MPa], ", 1.0e-6 * (ph + pres_elem[n0]));
               printf("Pe-base-res: %.3f [MPa]\n", 1.0e-6 * pres_elem[n0]);
          }

          /* Redo time step with the new broken bond. */

          update_fracture_perm();
          restore_current_pres();
          solve_for_fluid_pressure(t0, t1);

          if (counter % n_substep_output == 0) 
               write_results();

          print_list(list_of_burst_size, n_bursts, 
               "Burst size of cluster", "Current burst sizes");

          counter++;
     }

     /* Log mass balance. */

     log_mass_balance();

     /* Set event times for the current bursts. */

     for (i = 0; i < n_bursts; i++)
     {
          double fraction = (double) (i + 1) / (double) n_bursts;
          event_time2[n_events + i] = t0 + fraction *(t1 - t0);
     }

     /* Store the bursts created during the last time step. */

     for (i = 0; i < n_bursts; i++)
          add_to_list_of_event_size(list_of_burst_size[i], t1);

     /* Show burst sizes in three groups. */
     make_burst_size_elem();

     /* Analyze the fracture network. */

     if (is_network_analysis)
     {
          find_all_branch_levels();
          find_shreve_number_for_network(&max_shreve_number);
          find_strahler_number_for_network(&max_strahler_number);
          check_stream_orders();
     }
}


static void check_stream_orders(
     void)
{
     int b;
     for (b = 0; b < grid1->n_bonds; b++)
          if (topo_bond[b] < 0 and strahler_bond[b] > 0)
               printf("BOND: %d, strahler=%d\n", b, strahler_bond[b]);

}


static void log_mass_balance(
     void)
{
     fvm_make_bond_flow(pde_pres, darcy_flow_bond);
     fvm_do_mass_balance_for_domain(&massbal);
}


static void update_fracture_perm(
     void)
{
     int bond, elem, n;
     int counter1 = 0;
     int counter2 = 0;

     mark_chimneys_reaching_surface();

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];

          if (not is_close_to_surface_elem(elem))
               is_domain_elem[elem] = TRUE;
     }

     for (n = 0; n < n_frac_bonds; n++)
     {
          bond = list_of_frac_bonds[n];
          /* Default is closed fracture perm. */
          perm_bond[bond] = perm_frac_closed;

          if (is_open_fractured_bond(bond) and is_dynamic_frac_perm)
          {
               perm_bond[bond] = perm_frac_open;
               counter1++;
          }
          else
          {
               counter2++;
          }
     }

     printf("(CRITICAL bonds: %d, NON-critrical bonds: %d)\n",
          counter1, counter2);
}


static void mark_chimneys_reaching_surface(
     void)
{
     int size, elem, i;
     int *elem_list = NULL;
     int n_h_elems = fem_get_n_h_elems(grid1);

     ABC_NEW_ARRAY_INIT(elem_list, int, n_h_elems, -1);
     size = get_chimneys_reaching_surface(elem_list);

     for (elem = 0; elem < grid1->n_elems; elem++)
          is_surface_chimney_elem[elem] = FALSE;

     for (i = 0; i < size; i++)
     {
          elem = elem_list[i];
          mark_chimney_elems_until_reservoir(elem);
     }

     ABC_FREE_ARRAY(elem_list);
}


static void mark_chimney_elems_until_reservoir(
     int elem0)
{
     int i, size, elem, neighbor_elems[16];

     is_surface_chimney_elem[elem0] = TRUE;
     size = fem_get_nearest_neighbor_elements(grid1, elem0, neighbor_elems);

     for (i = 0; i < size; i++)
     {
          elem = neighbor_elems[i];
          if (elem == elem0) continue;
          if (not is_fractured_elem[elem]) continue;
          if (is_surface_chimney_elem[elem]) continue;
          mark_chimney_elems_until_reservoir(elem);
     }
}


static int get_most_critical_bond(
     void)
{
     int bond_min = -1;
     int n, i, dir, bond, size;
     int elem, elem1, elem2, bond_array[8];
     double stress_critical, stress_hH;
     double stress_min = 1.0e+12;
     double xmin = 1.0e+12;

     if (n_frac_elems > max_frac_elems)
     {
          printf("MAX FRAC ELEMS = %d IS EXCEEDED!\n", max_frac_elems); 
          return -1;
     }

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          fvm_get_bonds_for_elem(grid1, elem, bond_array, &size);

          for (i = 0; i < size; i++)
          {
               bond  = bond_array[i];
               dir   = dir_bond[bond];
               elem1 = grid1->bond_elem1[bond];
               elem2 = grid1->bond_elem2[bond];

               if (is_fractured_bond[bond])
                    continue;

               if (is_permeable_elem[elem1] and is_permeable_elem[elem2])
                    continue;

               if (is_top_surface_elem(elem1) or is_top_surface_elem(elem2))
                    continue;

               if (not is_permeable_elem[elem1] and not is_permeable_elem[elem2])
               {
                    printf("[get_weakest_bond...] This shouldn't happen! (elem=%d)\n", elem);
                    print_bond(bond);
                    exit(1);
               }

               if (is_permeable_elem[elem2])
                    abc_swap_ints(&elem1, &elem2);

               if (dir < 0 or 2 < dir)
               {
                    printf("I: bond=%d, dir=%d, n_bonds=%d\n", bond, dir, grid1->n_bonds);
                    fflush(stdout);
                    ABC_ERROR_EXIT("Bond direction is not 0, 1 or 2! (I)");
               }

               if (not is_OK_bond_dir(bond))
                    printf("Warning: Bond direction error!\n");

               stress_hH = get_least_eff_comp_stress(bond);
               stress_critical = stress_hH + frac_limit_bond[bond] - pres_elem[elem1];

               /***
               printf("DEBUG: n=%d, bond=%d, elem1=%d, is_res=%d, crit=%g, pres=%g\n",
                    n, bond, elem1, is_reservoir_elem[elem1], stress_critical, pres_elem[elem1]);
               ***/

               if (stress_critical < xmin)
                    xmin = stress_critical;

               if (stress_critical > 0.0) continue;

               if (stress_critical < stress_min)
               {
                    stress_min = stress_critical;
                    bond_min = bond;
               }
          }
     }

     /*** XXX ***/
     printf("(most critical stress: %g, %s)\n", 
          xmin, (xmin < 0.0) ? "FRACTURING" : "INTACT");
     return bond_min;
}


static int get_weakest_reservoir_bond(
     void)
{
     int bond_min = -1;
     int n, i, dir, bond, size;
     int elem, elem1, elem2, bond_array[8];
     double stress_critical, stress_hH;
     double stress_min =  1.0e+12;

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          if (not is_reservoir_elem[elem]) continue;
          fvm_get_bonds_for_elem(grid1, elem, bond_array, &size);

          for (i = 0; i < size; i++)
          {
               bond  = bond_array[i];
               dir   = dir_bond[bond];
               elem1 = grid1->bond_elem1[bond];
               elem2 = grid1->bond_elem2[bond];

               if (is_fractured_bond[bond])
                    continue;

               if (is_reservoir_elem[elem1] and is_reservoir_elem[elem2])
                    continue;

               if (not is_reservoir_elem[elem1] and not is_reservoir_elem[elem2])
               {
                    printf("[get_weakest_bond...] This shouldn't happen! (elem=%d)\n", elem);
                    print_bond(bond);
                    exit(1);
               }

               if (is_reservoir_elem[elem2])
                    abc_swap_ints(&elem1, &elem2);

               if (dir < 0 or 2 < dir)
               {
                    printf("III: bond=%d, dir=%d, n_bonds=%d\n", bond, dir, grid1->n_bonds);
                    fflush(stdout);
                    ABC_ERROR_EXIT("Bond direction is not 0, 1 or 2! (III)");
               }

               if (not is_OK_bond_dir(bond))
                    printf("Warning: Bond direction error!\n");

               stress_hH = get_least_eff_comp_stress(bond);
               stress_critical = stress_hH + frac_limit_bond[bond];

               if (stress_critical < stress_min)
               {
                    stress_min = stress_critical;
                    bond_min = bond;
               }
          }
     }

     return bond_min;
}


static double get_dt(
     void)
{
     double dt = t_end / (n_main_steps - 1);
     return dt;
}


static double get_critical_stress_bond(
     int bond)
{
     double stress_hH = get_least_eff_comp_stress(bond);
     double critical_stress = stress_hH + frac_limit_bond[bond];
     return critical_stress;
}


static double get_least_eff_comp_stress(
     int bond)
{
     int dir   = dir_bond[bond];
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];
     double stress_comp = sigma_h;
     double mean_lithostatic, mean_hydrostatic;

     if ((elem1 < 0) or (elem2 < 0))
          ABC_ERROR_EXIT("[get_least_eff_comp_stress] Illegal bond!");

     if (not has_lithostatic) 
          return sigma_h;

     if (is_fxy_by_eff_stress)
     {
          mean_lithostatic = 0.5 * (eff_lithostatic_elem[elem1] + eff_lithostatic_elem[elem2]);
          stress_comp = factor_min_hh[dir] * mean_lithostatic;
     }
     else
     {
          mean_lithostatic = 0.5 * (lithostatic_elem[elem1] + lithostatic_elem[elem2]);
          mean_hydrostatic = 0.5 * (hydrostatic_elem[elem1] + hydrostatic_elem[elem2]);
          stress_comp = factor_min_hh[dir] * mean_lithostatic - mean_hydrostatic;
     }

     return stress_comp;
}


static int is_vertical_top_bond(
     int bond)
{
     int dir = dir_bond[bond];
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];

     if (dir != 2) return FALSE;
     if (grid1->elem_info[elem1] & FEMBoundaryTop) return TRUE;
     if (grid1->elem_info[elem2] & FEMBoundaryTop) return TRUE;

     return FALSE;
}


static int is_top_surface_elem(
     int elem)
{
     if (elem < 0) return FALSE;
     if (grid1->elem_info[elem] & FEMBoundaryTop) return TRUE;
     return FALSE;
}


static int is_close_to_surface_elem(
     int elem)
{
     int n_z_elems = grid1->n_z_elems;
     int n_h_elems = fem_get_n_h_elems(grid1);
     int row  = elem / n_h_elems;
     if (row < n_z_elems - 2) return FALSE;
     return TRUE;
}


static int is_OK_bond_dir(
     int bond)
{
     int dir = dir_bond[bond];
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];
     double x1 = grid1->xcoord_elem[elem1];
     double x2 = grid1->xcoord_elem[elem2];
     double dx = ABC_ABS(x2 - x1);
     double y1 = grid1->ycoord_elem[elem1];
     double y2 = grid1->ycoord_elem[elem2];
     double dy = ABC_ABS(y2 - y1);
     double z1 = grid1->zcoord_elem[elem1];
     double z2 = grid1->zcoord_elem[elem2];
     double dz = ABC_ABS(z2 - z1);

     if (dir == 0)
     {
          if (dy < 0.001 and dz < 0.001) return TRUE;
          printf("bond=%d, dir=%d, y1=%g, y2=%g\n", bond, dir, y1, y2);                
          printf("bond=%d, dir=%d, z1=%g, z2=%g\n", bond, dir, z1, z2);
          return FALSE;
     }

     if (dir == 1)
     {
          if (dx < 0.001 and dz < 0.001) return TRUE;
          printf("bond=%d, dir=%d, x1=%g, x2=%g\n", bond, dir, x1, x2); 
          printf("bond=%d, dir=%d, z1=%g, z2=%g\n", bond, dir, z1, z2);                
          return FALSE;
     }

     if (dir == 2)
     {
          if (dx < 0.001 and dy < 0.001) return TRUE;
          printf("bond=%d, dir=%d, x1=%g, x2=%g\n", bond, dir, x1, x2); 
          printf("bond=%d, dir=%d, y1=%g, y2=%g\n", bond, dir, y1, y2);                
          return FALSE;
     }

     printf("THIS should not happen!!\n");
     return FALSE;
}


static int is_open_fractured_bond(
     int bond)
{
     switch (open_bond_mode)
     {
          case OPEN_CHIMNEY_BOND:  return is_open_chimney_bond(bond);
          case OPEN_CRITICAL_BOND: return is_open_critical_bond(bond);
          default: ABC_ERROR_EXIT("No such open bond mode!");
     }

     return FALSE;
}


static int is_open_chimney_bond(
     int bond)
{    
     int elem1, elem2;
     
     elem1 = grid1->bond_elem1[bond];
     elem2 = grid1->bond_elem2[bond];

     if (elem1 < 0) return FALSE;
     if (elem2 < 0) return FALSE;
     
     if (not is_fractured_elem[elem1]) return FALSE;
     if (not is_fractured_elem[elem2]) return FALSE;

     if (is_surface_chimney_elem[elem1]) return FALSE;
     if (is_surface_chimney_elem[elem2]) return FALSE;

     return TRUE;
}


static int is_open_critical_bond(
     int bond)
{
     int elem1, elem2;
     double pres, stress_hH, stress_critical;

     elem1 = grid1->bond_elem1[bond];
     elem2 = grid1->bond_elem2[bond];

     if (not is_fractured_elem[elem1]) return FALSE;
     if (not is_fractured_elem[elem2]) return FALSE;

     pres = 0.5 * (pres_elem[elem1] + pres_elem[elem2]);

     stress_hH = get_least_eff_comp_stress(bond);
     stress_critical = stress_hH + frac_limit_bond[bond] - pres;

     if (stress_critical <= 0.0) return TRUE;

     return FALSE;
}


static void break_the_bond(
     int bond,
     double t1)
{
     int k, elem12[2];

     if (is_fractured_bond[bond])
          return;

     elem12[0] = grid1->bond_elem1[bond];
     elem12[1] = grid1->bond_elem2[bond];

     for (k = 0; k < 2; k++)
     {
          /* Is the elem already in the frac-elem-list? */
          if (is_fractured_elem[elem12[k]]) continue;

          add_to_list_of_fractured_elements(elem12[k]);

          /* Reservoir elems are already in the perm-elem-list! */
          if (not is_reservoir_elem[elem12[k]])
               add_to_list_of_permeable_elements(elem12[k]);
     }

     add_to_list_of_fractured_bonds(bond, t1);
     /*** XXX ***/
     add_to_list_of_bursts(bond);

     if (not is_loopless)
     {
          check_for_intact_bonds(elem12[0]);
          check_for_intact_bonds(elem12[1]);
     }
}


static void load_current_pres(void)
{
     ABC_COPY_ARRAY(pde_pres->solution1->value, pres_elem, grid1->n_elems);
}


static void save_current_pres(
     void)
{
     ABC_COPY_ARRAY(pde_pres->solution1->value, pres_saved_elem, grid1->n_elems);
}


static void restore_current_pres(
     void)
{
     ABC_COPY_ARRAY(pres_saved_elem, pde_pres->solution0->value, grid1->n_elems);
     ABC_COPY_ARRAY(pres_saved_elem, pde_pres->solution1->value, grid1->n_elems);
}


static void plot_pres_profile(
     double t1)
{
     static FILE *out = NULL;
     int n_x_elems = grid1->n_x_elems;
     int row = first_frac_elem / n_x_elems;
     int elem1 = row * n_x_elems;
     int elem;

     if (first_frac_elem < 0) return;

     if (out == NULL)
     {
          out = abc_open_listed_file("x1-prof.csv", "x-profile at time steps:", "w");

          fprintf(out, "#(\n");
          fprintf(out, "setvarname{buffer= 0, name=\"xcoord_elem\"}\n");
          fprintf(out, "setvarname{buffer= 1, name=\"pres_elem\"}\n");
          fprintf(out, ")#\n");
     }

     fprintf(out, "#( time: %g [s] )#\n", t1);
     for (elem = elem1; elem < elem1 + n_x_elems; elem++)
          fprintf(out, "%g %g\n", 
               grid1->xcoord_elem[elem], pres_elem[elem]);

     fflush(out);
}

/*
**   ==============================
**   The invasion percolation model
**   ==============================
*/

static void run_IP_test(
     void)
{
     int step, bond_start;

     print_list(list_of_frac_elems, n_frac_elems, 
          "Element of index", "Fractured elements");

     for (step = 0; step < n_main_steps; step++)
     {
          do_one_time_step(step, &bond_start);

          if (bond_start < 0) 
          {
               printf("(no more weak bonds?)\n");
               return;
          }

          fracture_bonds(step, bond_start);

          if (is_sample_spanning)
          {
               printf("STOPPED: Sample spanning cluster!\n");
               break;
          }
     }

     write_results();
}


static void do_one_time_step(
     int step,
     int *bond_start)
{
     printf("------------------\n");
     printf("NEW STEP (step=%d)\n", step);

     *bond_start = get_weakest_bond_from_fracture();
}


static int get_weakest_bond_from_fracture(
     void)
{
     int n, i, bond, elem, elem1, elem2, size, bond_array[8];
     int b_min = -1;
     double f_min = 1.0e+12;

     for (n = 0; n < n_frac_elems; n++)
     {
          elem = list_of_frac_elems[n];
          fvm_get_bonds_for_elem(grid1, elem, bond_array, &size);

          for (i = 0; i < size; i++)
          {
               bond = bond_array[i];
               elem1 = grid1->bond_elem1[bond];
               elem2 = grid1->bond_elem2[bond];

               if (is_fractured_bond[bond]) 
                    continue;

               if (is_fractured_elem[elem1] and is_fractured_elem[elem2]) 
                    continue;

               if (not is_fractured_elem[elem1] and not is_fractured_elem[elem2]) 
               {
                    printf("[get_weakest_bond...] This shouldn't happen! (elem=%d)\n", elem);
                    print_bond(bond);
                    exit(1);
               }

               if (is_fractured_elem[elem2])
                    abc_swap_ints(&elem1, &elem2);

               /***
               print_bond(bond);
               ***/

               if (frac_limit_bond[bond] < f_min)
               {
                    f_min = frac_limit_bond[bond];
                    b_min = bond;
               }
          }
     }

     return b_min;
}

/*
**   ================
**   Common functions
**   ================
*/

static void solve_for_fluid_pressure(
     double t0,
     double t1)
{
     switch (solver_type)
     {
          case SOLVER_SCALAR_PRES:
          make_fast_pres_solution(t1);
          break;

          case SOLVER_NETWORK:
          make_network_solution(t0, t1);
          load_current_pres();
          break;

          case SOLVER_FULL_GRID:
          fvm_do_time_step(pde_pres, t0, t1);
          load_current_pres();
          break;

          default:
          ABC_ERROR_EXIT("Unknown solver!");
     }
}


static void make_network_solution(
     double t0,
     double t1)
{
     if (ns == NULL) 
     {
          ns = fvm_create_network_solver(pde_pres, 
               is_permeable_elem, 
               make_elem_coefs_ns, 
               make_bond_coefs_ns, 
               AbcMatrixTypeSparse);

          init_reservoir_pres();
     }

     fvm_network_solve(ns, t0, t1);

     /***
     fvm_show_network_solution(ns);
     ***/
}


static void make_bond_coefs_ns(
     FEMDiffEq *pde,
     int b1,
     FEMBondCoefs *coefs)
{
     int b2 = ns->map_bond_new_to_old[b1];

     coefs->diffusivity = perm_bond[b2] / visc0;
     coefs->have_velocity_term = FALSE;
     coefs->velocity = 0.0;

     if (is_vertical_top_bond(b2))
     {
          /***
          printf("b2=%d is top bond!\n", b2);
          ***/
          coefs->diffusivity = 1.0e-8 / visc0;
     }

     /***
     if (is_fractured_bond[b2])
          printf("PERM bond %d: %g, depth: %g\n", 
               b2, perm_bond[b2], get_depth_of_bond(b2));
     ***/

     /***
     printf("b-new=%d, b-old=%d, perm=%g, visc=%g, main=%d: ", 
          b1, b2, perm_bond[b2],visc0,
          fvm_is_main_bond(ns, b2)); 
     fvm_print_network_one_bond_old(ns, b2, "make-bond");
     ***/

     ABC_UNUSED_PARAMETER(pde);
}


static void make_elem_coefs_ns(
     FEMDiffEq *pde,
     FEMType *fem,
     FEMCoefs *coef)
{
     int elem1 = fem->element;
     int elem2 = ns->map_elem_new_to_old[elem1];
     /***
     double rate = get_reservoir_rate_per_vol();
     ***/
     double rate = get_source_term(elem2);

     /* Part 1: Storage */

     coef->have_storage_term = TRUE;
     coef->storage = phi_elem[elem2] * compres0;

     /* Part 2: Diffusivity */

     coef->have_velocity_term = FALSE;

     coef->diffusivity[0][0] = 1.0;
     coef->diffusivity[1][1] = 1.0;
     coef->diffusivity[2][2] = 1.0;

     /* Part 3: Source */

     coef->have_source_term = FALSE;
     coef->source = 0.0;

     if (not is_reservoir_elem[elem2]) return;

     coef->have_source_term = TRUE;
     coef->source = rate;

     /***
     if (elem2 == 100)
          printf("HAS SOURCE!! (elem=%d, source=%g, rate=%g\n", 
               elem2, coef->source,
               get_reservoir_rate_per_vol());
     ***/

     ABC_UNUSED_PARAMETER(pde);
}


static void make_fast_pres_solution(
     double t1)
{
     int n, elem;
     double pres = pres_first_frac;

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          pres_elem[elem] = pres;
     }

     ABC_UNUSED_PARAMETER(t1);
}


static double get_init_reservoir_pres(
     void)
{
     double pres = pfac1 * pres_first_frac;
     return pres;
}


static double get_final_reservoir_pres(
     void)
{
     double pres = pfac2 * pres_first_frac;
     return pres;
}


static double get_first_frac_overpres(
     void)
{
     int bond = get_weakest_reservoir_bond();
     double critical_stress = get_critical_stress_bond(bond);
     return critical_stress;
}


static double get_max_overpres(
     void)
{
     double delta_dens = dens_bulk - dens_fluid;
     double hmax = depth_top_surf - size_vertical;
     double pmax = delta_dens * gravity * hmax;
     /***
     printf("(pmax=%g [Pa])\n", pmax);
     ***/
     return pmax;
}


static double get_source_term(
     int elem)
{
     int n_h_elems = fem_get_n_h_elems(grid1);
     if (not is_reservoir_elem[elem]) return 0.0;
     if (not is_base_source) return source_res;
     if (elem / n_h_elems != 0) return 0.0;
     return source_base;
}


static double get_reservoir_rate_per_vol(
     void)
{
     double pres1 = get_init_reservoir_pres();
     double pres2 = get_final_reservoir_pres();
     double dpres = pres2 - pres1;
     double rate = phi_res * compres0 * dpres / t_end;
     return rate;
}

static double get_first_frac_ph(
     void)
{
     double ph;
     if (first_frac_elem < 0) return 0.0;
     ph = dens_fluid * gravity * grid1->zcoord_elem[first_frac_elem];
     return ph;
}


static double get_average_res_pres(
     void)
{
     int n, elem;
     int counter = 0;
     double sum = 0.0;
     
     for (n = 0; n < n_perm_elems; n++)
     {    
          elem = list_of_perm_elems[n];
          if (not is_reservoir_elem[elem]) continue;
          sum += pres_elem[elem];
          counter += 1;
     }

     if (counter > 0) 
          sum /= counter;

     return sum;
}


static double get_compressed_vol_in_res(
     void)
{
     int n, elem;
     double dV, V_pore;
     double sum = 0.0;

     /* 
     ** Notice: pres_elem is the overpressure in the element.
     ** The compression of fluid starts at zero overpressure.
     */

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          if (not is_reservoir_elem[elem]) continue;
          V_pore = phi_res * grid1->volume_elem[elem];
          dV = V_pore * compres0 * pres_elem[elem];
          sum += dV;
     }

     return sum;
}


static double get_injected_vol(
     void)
{
     return 0.0;
}


static double get_total_pore_vol(
     void)
{
     int n, elem;
     double dV_pore;
     double sum = 0.0;

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_frac_elems[n];
          dV_pore = phi_elem[elem] * grid1->volume_elem[elem];
          sum += dV_pore;
     }

     return sum;
}


static double get_bulk_reservoir_vol(
     void)
{
     int n, elem;
     double vol = 0.0;

     for (n = 0; n < n_perm_elems; n++)
     {
          elem = list_of_perm_elems[n];
          if (not is_reservoir_elem[elem]) continue;
          vol += grid1->volume_elem[elem];
     }

     return vol;
}


static double get_total_volume(
    void)
{
     double V = size_lateral * size_lateral * size_vertical;
     return V;
}


static double get_cell_volume(
     void)
{
     double V = get_total_volume();
     double dV = V / grid1->n_elems;
     return dV;
}


static double get_max_dist_from_well(
    void)
{
    double l1 = size_lateral / 2.0;
    double l2 = l1 * l1;
    double dist = sqrt(l2 + l2 + l2); 
    return dist;
}


static double get_max_dist_to_frac_elem(
     void)
{
     int elem;
     double dist;
     double max_dist = 0.0;

     if (n_events < 1) return 0.0;
     if (first_frac_elem < 0) return 0.0;

     for (elem = 0; elem < grid1->n_elems; elem++)
     {
          if (not is_fractured_elem[elem]) continue;
          dist = fem_get_elem_distance(grid1, first_frac_elem, elem);
          if (dist > max_dist) max_dist = dist;
     }

     return max_dist;
}


static void print_separator(
     FILE *out)
{
     fprintf(out, "=============================\n");
}


static void print_bond(
     int bond)
{
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];

     if (is_fractured_elem[elem2])
          abc_swap_ints(&elem1, &elem2);

     printf("(bond=%3d, elem1=%3d (%s), elem2=%3d (%s), strength=%.4f)\n",
          bond, 
          elem1, get_code_name_for_elem(is_fractured_elem[elem1]),
          elem2, get_code_name_for_elem(is_fractured_elem[elem2]),
          frac_limit_bond[bond]);
}


static const char *get_YES_or_NO(
     int is_yes)
{
     return (is_yes) ? "YES" : "NO";
}


static const char *get_code_name_for_elem(
     int code)
{
     return (code == 0) ? "ROCK" : "FRAC";
}


static void check_for_intact_bonds(
     int elem)
{
     /*
     ** Check for intact bonds that connect two fractured elements.
     */

     double t1 = pde_pres->t1;
     int i, b, e1, e2, size;
     int bonds[32];

     if (not is_fractured_elem[elem])
          ABC_ERROR_EXIT("[check_for_intact_bonds] Shouldn't be called!");

     fvm_get_bonds_for_elem(grid1, elem, bonds, &size);

     for (i = 0; i < size; i++)
     {
          b  = bonds[i];
          e1 = grid1->bond_elem1[b];
          e2 = grid1->bond_elem2[b];

          if (is_fractured_bond[b]) continue;
          if (e1 != elem) abc_swap_ints(&e1, &e2);
          if (e1 != elem) printf("ERROR: e1 != ELEM\n");
          if (e2 < 0) continue;
          if (not is_fractured_elem[e2]) continue;

          printf("EXTRA bond: %d becomes fractured!\n", b);
          add_to_list_of_fractured_bonds(b, t1);
     }
}


static void fracture_bonds(
     int step,
     int bond_start)
{
     int total_events = 0;
     int broken_bonds = 0;
     int elem1 = grid1->bond_elem1[bond_start];
     int elem2 = grid1->bond_elem2[bond_start];
     double fluid_pres = 1.05 * frac_limit_bond[bond_start];
     double fraction;

     printf("(starting with bond: %3d and fluid pressure: %g)\n", 
          bond_start, fluid_pres);

     if (not is_fractured_elem[elem1])
          abc_swap_ints(&elem1, &elem2);

     if (not is_fractured_elem[elem1])
          ABC_ERROR_EXIT("[fracture_bond] Illegal element!");

     try_to_break_elements_from(elem1, n_events, &broken_bonds, fluid_pres);

     if (broken_bonds > 0) 
          add_to_list_of_event_size(broken_bonds, 0.0);

     total_events += broken_bonds;
     fraction = ((double) total_events / (double) grid1->n_elems);

     if (is_verbose)
          printf("STEP: %4d, broken: %3d, fraction: %6.3f\n", 
               step, broken_bonds, fraction);

     write_to_log_file(step, broken_bonds, fluid_pres);
}


static void add_to_list_of_event_size(
     int broken_bonds,
     double t1)
{
     if (n_events >= MAX_EVENTS)
          ABC_ERROR_RETURN("Too many events!");

     if (broken_bonds < 1)
          return;

     list_of_event_size[n_events] = broken_bonds;
     event_time1[n_events] = t1;
     n_events++;
}


static void add_to_list_of_fractured_elements(
     int elem)
{
     if (n_frac_elems >= grid1->n_elems)
          ABC_ERROR_RETURN("Too many fractured elements!");

     if (is_fractured_elem[elem])
          ABC_ERROR_RETURN("Element is already fractured!");

     is_fractured_elem[elem] = TRUE;
     list_of_frac_elems[n_frac_elems] = elem;
     n_frac_elems++;
}


static void add_to_list_of_permeable_elements(
     int elem)
{
     if (n_perm_elems >= grid1->n_elems)
          ABC_ERROR_RETURN("Too many permeable elements!");

     if (is_permeable_elem[elem])
     {
          printf("XXX: elem=%d is already permeable!\n", elem);
          if (is_reservoir_elem[elem])
          printf("XXX: elem=%d is also a reservoir element!\n", elem);
     }

     if (is_permeable_elem[elem])
          ABC_ERROR_RETURN("Element is already permeable!");

     is_permeable_elem[elem] = TRUE;
     list_of_perm_elems[n_perm_elems] = elem;
     n_perm_elems++;
}


static void add_to_list_of_fractured_bonds(
     int bond,
     double t1)
{
     if (n_frac_bonds >= grid1->n_bonds)
          ABC_ERROR_RETURN("Too many fractured bonds!");

     if (is_fractured_bond[bond])
          ABC_ERROR_RETURN("Bond is already fractured!");

     perm_bond[bond] = perm_frac_open;
     time_bond[bond] = t1;
     is_fractured_bond[bond] = TRUE;
     list_of_frac_bonds[n_frac_bonds] = bond;
     n_frac_bonds++;
}


static void add_to_list_of_bursts(
     int bond)
{
     /*
     ** A bond connects two neighbour elements. The newly broken bond
     ** connects a previously fractured element with its newly fractured
     ** neighbour element.  The previously fractured element belongs to
     ** a burst.  It has a burst number. The newly fractured element
     ** becomes part of the same burst as the fracture element it is
     ** connected to. Therefore, the newly fractured element gets the
     ** same burst number. For each burst number, there is a counter
     ** for the number of fractured elements in the burst.
     ** 
     ** Furthermore, all elements in a burst linked by a list.
     ** It is therefore possible to plot all elements that belong
     ** to the same burst.
     */

     int burst_no = -1;
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];

     /* The remaining part is for fractured elements. */

     if      (burst_elem[elem1] >= 0 and burst_elem[elem2] <  0)
     {
          burst_no = burst_elem[elem1];
          list_of_burst_size[burst_no] += 1;
          add_to_list_of_elems_in_bursts(burst_no, elem2);
     }
     else if (burst_elem[elem1] <  0 and burst_elem[elem2] >= 0)
     {
          burst_no = burst_elem[elem2];
          list_of_burst_size[burst_no] += 1;
          add_to_list_of_elems_in_bursts(burst_no, elem1);
     }
     else if (burst_elem[elem1] <  0 and burst_elem[elem2] <  0)
     {
          burst_no = n_bursts;
          list_of_burst_size[burst_no] += 1;
          add_to_list_of_elems_in_bursts(burst_no, elem1);
          add_to_list_of_elems_in_bursts(burst_no, elem2);
          n_bursts++;
     }
     else
     {
          printf("[add_to_list_of_burts] Error!");
     }

     if (burst_no < 0)
          ABC_ERROR_EXIT("[add_to_list_of_burts] Can't happend!");

     burst_elem[elem1] = burst_no;
     burst_elem[elem2] = burst_no;
}


static void add_to_list_of_elems_in_bursts(
     int burst_no,
     int elem)
{
     int i = n_events + burst_no;
     int elem0 = first_elem_in_burst[i];
     first_elem_in_burst[i] = elem;
     next_elem_in_burst[elem] = elem0;
}


static void print_list(
     int *list,
     int size,
     const char *name,
     const char *title)
{
     int i;

     printf("List: %s\n", title);
     for (i = 0; i < size; i++)
          printf("%s %3d is %3d\n", name, i, list[i]);
}


static void try_to_break_elements_from(
     int elem0,
     int burst_id,
     int *broken_bonds,
     double fluid_pres)
{
     static int level = 0;
     int i, bond, elem1, elem2, size, bond_array[8];

     if (is_sample_spanning) return;

     level++;

     if (is_debugging)
          printf("(try_to_break: BEGIN LEVEL=%d, ELEM=%d)\n", level, elem0);

     fvm_get_bonds_for_elem(grid1, elem0, bond_array, &size);

     for (i = 0; i < size; i++)
     {
          bond = bond_array[i];

          if (is_fractured_bond[bond])
               continue;

          elem1 = grid1->bond_elem1[bond];
          elem2 = grid1->bond_elem2[bond];

          /* Error: A bond between wrong elements! */
          if ((elem1 != elem0) and (elem2 != elem0))
               ABC_ERROR_EXIT("[try_to_break_elements_from] Bond error!");

          /* Error: A bond between to non-fractured elements. */
          if (not is_fractured_elem[elem1] and not is_fractured_elem[elem2])
               ABC_ERROR_EXIT("[try_to_break_elements_from] Bond error!");

          if (elem2 == elem0) 
               abc_swap_ints(&elem1, &elem2);

          /* Error: A bond between to non-fractured elements. */
          if (not is_fractured_elem[elem1] and is_fractured_elem[elem2])
               ABC_ERROR_EXIT("[try_to_break_elements_from] Bond error!");

          /* This is a bond between fractured elements. */
          if (is_fractured_elem[elem1] and is_fractured_elem[elem2])
               continue;

          if (is_debugging)
               print_bond(bond);

          if (frac_limit_bond[bond] < fluid_pres)
          {
               if (is_debugging)
                    printf("(BROKEN: bond=%d element=%d)\n", bond, elem2);

               *broken_bonds += 1;
               burst_elem[elem2] = burst_id;
               add_to_list_of_fractured_bonds(bond, 0.0);
               add_to_list_of_fractured_elements(elem2);

               /* Mark bond and element as fractured. */
               is_fractured_bond[bond] = TRUE;
               is_fractured_elem[elem2] = TRUE;

               /* Check if the last element connects opposite sides. */ 
               /* Continue recursively until a solid element. */

               is_sample_spanning = is_sample_spanning_elem(elem2);

               if (not is_sample_spanning)
                    try_to_break_elements_from(elem2, burst_id, broken_bonds, fluid_pres);
          }
     }

     if (is_debugging)
          printf("(try_to_break: END LEVEL=%d, ELEM=%d)\n", level, elem0);

     level--;
}


static int is_sample_spanning_elem(
     int elem)
{
     int is_on_left_boundary = FALSE;
     int is_on_right_boundary = FALSE;
     int is_on_top_boundary = FALSE;
     int is_on_bot_boundary = FALSE;
     int is_on_back_boundary = FALSE;
     int is_on_front_boundary = FALSE;

     if (grid1->elem_info[elem] & FEMBoundaryLeft)  is_on_left_boundary = TRUE;
     if (grid1->elem_info[elem] & FEMBoundaryRight) is_on_right_boundary = TRUE;
     if (grid1->elem_info[elem] & FEMBoundaryTop)   is_on_top_boundary = TRUE;
     if (grid1->elem_info[elem] & FEMBoundaryBot)   is_on_bot_boundary = TRUE;
     if (grid1->elem_info[elem] & FEMBoundaryBack)  is_on_back_boundary = TRUE;
     if (grid1->elem_info[elem] & FEMBoundaryFront) is_on_front_boundary = TRUE;

     if (is_on_left_boundary and is_on_right_boundary) return TRUE;
     if (is_on_bot_boundary  and is_on_top_boundary)   return TRUE;
     if (is_on_back_boundary and is_on_front_boundary) return TRUE;

     return FALSE;
}


static void find_all_branch_levels(
     void)
{    
     if (first_frac_elem < 0) return;
     printf("(finding branch levels)\n");
     abc_init_ints(topo_bond, grid1->n_bonds, -1);
     find_all_branch_levels_from_elem(first_frac_elem, 1);
     max_topo_number = get_max_topo_number();
}


static void find_all_branch_levels_from_elem(
     int elem0,
     int cur_level)
{
     int i, size, next_level, bonds[8], elems2[8];

     find_all_fractured_bonds_from_elem(elem0, bonds, elems2, &size);

     if (size < 1) return;
     else if (size <= 2) next_level = cur_level;
     else next_level = cur_level + 1;

     for (i = 0; i < size; i++)
     {
          if (topo_bond[bonds[i]] >= 0) continue;
          topo_bond[bonds[i]] = next_level;
          find_all_branch_levels_from_elem(elems2[i], next_level);
     }
}


static void find_all_fractured_bonds_from_elem(
     int elem,
     int *bonds2,
     int *elems2,
     int *size2)
{
     int i, b, e1, e2, size, bonds[8];
     int n = 0;

     fvm_get_bonds_for_elem(grid1, elem, bonds, &size);

     for (i = 0; i < size; i++)
     {
          b  = bonds[i];
          if (not is_fractured_bond[b]) continue;
          e1 = grid1->bond_elem1[b];
          e2 = grid1->bond_elem2[b];
          if (e1 != elem) abc_swap_ints(&e1, &e2);
          if (e1 != elem) printf("[find_all_fractured_bonds...] Error!");
          bonds2[n] = b;
          elems2[n] = e2;
          n += 1;
     }

     *size2 = n;
}


static void find_longest_branch(
     int *list2_bond,
     int *size2,
     int max_list2_size)
{
     int size, prev_elem, prev_bond, next_elem, next_bond;
     int list_bond[MAX_LIST_SIZE], list_elem[MAX_LIST_SIZE];
     int max_level = get_max_topo_number();
     int n = 0;

     if (first_frac_elem < 0) return;

     find_dead_end_bond_and_elem(max_level, list_bond, list_elem, &size, MAX_LIST_SIZE);

     if (size < 1) ABC_ERROR_EXIT("No dead-ends?");

     prev_bond = list_bond[0];
     prev_elem = fvm_get_conjugate_bond_elem(grid1, prev_bond, list_elem[0]);

     list2_bond[n] = prev_bond;
     n++;

     while (prev_elem != first_frac_elem)
     {
         if (not find_next_bond_and_elem(prev_bond, prev_elem, &next_bond, &next_elem)) break;
         if (n >= max_list2_size) {*size2 = n; return;}

         list2_bond[n] = next_bond;
         n++;

         prev_elem = next_elem;
         prev_bond = next_bond;
     }

     *size2 = n;
}


static void find_shreve_number_for_network(
     int *smax)
{    
     int s0;

     if (first_frac_elem < 0) return;
     
     printf("(finding Shreve numbers)\n");
     abc_init_ints(shreve_bond, grid1->n_bonds, -1);
     s0 = find_shreve_number_from_elem(first_frac_elem, -1);
     printf("(max Shreve number = %d)\n", s0);
     *smax = s0;
}


static int find_shreve_number_from_elem(
     int elem0,
     int prev_bond)
{    
     int i, size, s0, sn[8], bonds[8], elems2[8];

     find_all_fractured_bonds_from_elem(elem0, bonds, elems2, &size);
     
     if (size < 2 and elem0 != first_frac_elem) return 1;
     
     for (i = 0; i < size; i++)
     {    
          sn[i] = 0;
          if (bonds[i] == prev_bond) continue;
          sn[i] = find_shreve_number_from_elem(elems2[i], bonds[i]);
          shreve_bond[bonds[i]] = sn[i];
     }
     
     s0 = get_downstream_shreve_number(sn, size);
     
     return s0;
}


static int get_downstream_shreve_number(
     int *sn,
     int size)
{
     int i, sum = 0;
     if (size < 1) ABC_ERROR_EXIT("[get_downstream...] Size error!");
     for (i = 0; i < size; i++) sum += sn[i];
     return sum;
}


static void find_strahler_number_for_network(
     int *smax)
{
     int s0;

     if (first_frac_elem < 0) return;

     printf("(finding Strahler numbers)\n");
     abc_init_ints(strahler_bond, grid1->n_bonds, -1);
     s0 = find_strahler_number_from_elem(first_frac_elem, -1);
     printf("(max Strahler number = %d)\n", s0);
     *smax = s0;
}


static int find_strahler_number_from_elem(
     int elem0,
     int prev_bond)
{
     int i, size, s0, sn[8], bonds[8], elems2[8];

     find_all_fractured_bonds_from_elem(elem0, bonds, elems2, &size);
     
     if (size < 2 and elem0 != first_frac_elem) return 1;

     for (i = 0; i < size; i++)
     {
          sn[i] = 0;
          if (bonds[i] == prev_bond) continue;
          sn[i] = find_strahler_number_from_elem(elems2[i], bonds[i]);
          strahler_bond[bonds[i]] = sn[i];
     }
    
     s0 = get_downstream_strahler_number(sn, size);

     return s0;
}


static int get_downstream_strahler_number(
     int *sn,
     int size)
{
     if (size < 1) ABC_ERROR_EXIT("[get_downstream...] Size error!");
     if (size == 1) return sn[0];
     qsort((void *) sn, (size_t) size, (size_t) sizeof(int), comp_reverse_ints);
     /*****
     print_sn(sn, size);
     *****/
     if (sn[0] == sn[1]) return (sn[0] + 1);
     return sn[0];
}


static int comp_reverse_ints(
     const void *a0,
     const void *b0)
{    
     int *a = (int *) a0;
     int *b = (int *) b0;
     return (*b - *a);
}


static int find_next_bond_and_elem(
     int prev_bond,
     int prev_elem,
     int *next_bond,
     int *next_elem)
{
     int cur_level = 999999999;
     int i, size, bonds[8], elems2[8], cur_bond, cur_elem;
     
     find_all_fractured_bonds_from_elem(prev_elem, bonds, elems2, &size);

     for (i = 0; i < size; i++)
     {
          int b = bonds[i];
          if (b == prev_bond) continue;
          if (not is_fractured_bond[b]) continue;
          if (topo_bond[b] > cur_level) continue;
          cur_level = topo_bond[b];
          cur_elem = elems2[i];
          cur_bond = b;
     }

     if (cur_bond < 0) return FALSE;
     
     *next_bond = cur_bond;
     *next_elem = cur_elem;

     return TRUE;
}


static void find_dead_end_bond_and_elem(
     int max_level,
     int *list_bond,
     int *list_elem,
     int *size,
     int max_list_size)
{
     int n = 0;
     int bond, elem;

     for (bond = 0; bond < grid1->n_bonds; bond++)
     {
          if (topo_bond[bond] != max_level) continue;
          elem = get_dead_end_elem(bond);
          if (elem < 0) continue;
          if (n >= max_list_size) break;
          list_elem[n] = elem;
          list_bond[n] = bond;
          n++;
     }

     *size = n;
}


static int get_dead_end_elem(
     int b)
{
     int size, bonds[8], elems2[8];
     int e1 = grid1->bond_elem1[b];
     int e2 = grid1->bond_elem2[b];

     find_all_fractured_bonds_from_elem(e1, bonds, elems2, &size);
     if (size == 1) return e1;

     find_all_fractured_bonds_from_elem(e2, bonds, elems2, &size);
     if (size == 1) return e2;

     return -1;
}


static int get_max_topo_number(
     void)
{
     int b;
     int max_level = -1;

     for (b = 0; b < grid1->n_bonds; b++)
          if (topo_bond[b] > max_level)
               max_level = topo_bond[b];

     return max_level;
}


static void find_bond_dir_weights(
     int sum[3],
     double weight[3])
{
     int i, errors = 0;
     double sum_total = 0.0;

     sum[0] = 0;
     sum[1] = 0;
     sum[2] = 0;

     weight[0] = 0.0;
     weight[1] = 0.0;
     weight[2] = 0.0;

     for (i = 0; i < n_frac_bonds; i++)
     {
          int b = list_of_frac_bonds[i];
          int d = dir_bond[b];

          if (d < 0 or 2 < d)
          {
               errors += 1;
               continue;
          }
          
          sum[d] += 1;
     }

     if (errors > 0)
          printf("[find_bond_dir_weights] %d illegal directions!\n", errors);
 
     sum_total = sum[0] + sum[1] + sum[2];

     if (sum_total < 1) return;

     weight[0] = ((double) sum[0]) / sum_total;
     weight[1] = ((double) sum[1]) / sum_total;
     weight[2] = ((double) sum[2]) / sum_total;
}


static void show_info(
     FILE *out)
{
     double min_hh = get_min_factor_hh();
     double fraction = (double) n_frac_elems / (double) grid1->n_elems;
     double max_dist1 = get_max_dist_to_frac_elem();
     double max_dist2 = get_max_dist_from_well();
     double delta_dens = dens_bulk - dens_fluid;

     double eff_sigma_zz_top = delta_dens * gravity * depth_top_surf;
     double eff_sigma_zz_bot = delta_dens * gravity * (depth_top_surf - size_vertical);
     double eff_sigma_zz_mid = (eff_sigma_zz_top + eff_sigma_zz_bot) / 2.0;
     double eff_sigma_zz_ctr = eff_lithostatic_elem[first_frac_elem];

     double eff_comp_top = min_hh * eff_sigma_zz_top;
     double eff_comp_mid = min_hh * eff_sigma_zz_mid;
     double eff_comp_bot = min_hh * eff_sigma_zz_bot;

     double sigma_zz_top = dens_bulk * gravity * depth_top_surf;
     double sigma_zz_bot = dens_bulk * gravity * (depth_top_surf - size_vertical);
     double sigma_zz_mid = (sigma_zz_top + sigma_zz_bot) / 2.0;
     double sigma_zz_ctr = eff_lithostatic_elem[first_frac_elem] + get_first_frac_ph();

     double ph_top = sigma_zz_top - eff_sigma_zz_top;
     double ph_mid = sigma_zz_mid - eff_sigma_zz_mid;
     double ph_bot = sigma_zz_bot - eff_sigma_zz_bot;

     double comp_top = min_hh * sigma_zz_top;
     double comp_mid = min_hh * sigma_zz_mid;
     double comp_bot = min_hh * sigma_zz_bot;

     double bond_weight[3];
     int bond_sum[3];

     find_bond_dir_weights(bond_sum, bond_weight);

     if (not has_lithostatic)
     {
          eff_sigma_zz_top = sigma_h;
          eff_sigma_zz_mid = sigma_h;
          eff_sigma_zz_bot = sigma_h;
     }

     if (out == stdout) print_separator(out);

     fprintf(out, "info: nodes = [%d x %d x %d] = %d\n", 
          grid1->n_x_nodes, grid1->n_y_nodes, grid1->n_z_nodes, grid1->n_nodes);
     fprintf(out, "info: elements = [%d x %d x %d] = %d\n", 
          grid1->n_x_elems, grid1->n_y_elems, grid1->n_z_elems, grid1->n_elems);
     fprintf(out, "info: phi reservoir = %g\n", phi_res);
     fprintf(out, "info: phi caprock = %g\n", phi_cap);
     fprintf(out, "info: phi damaged rock = %g\n", phi_dam);
     fprintf(out, "info: phi universal = %g\n", phi0);
     fprintf(out, "info: perm rock = %g m2\n", perm_rock);
     fprintf(out, "info: perm frac open = %g m2\n", perm_frac_open);
     fprintf(out, "info: perm frac closed = %g m2\n", perm_frac_closed);
     fprintf(out, "info: is dynamic frac perm = %d\n", is_dynamic_frac_perm);
     fprintf(out, "info: broken bonds = %d\n", n_frac_bonds);
     fprintf(out, "info: broken elements = %d\n", n_frac_elems);
     fprintf(out, "info: fraction of elements that are broken = %g\n", fraction);
     fprintf(out, "info: max topological number = %d\n", max_topo_number);
     fprintf(out, "info: max shreve number = %d\n", max_shreve_number);
     fprintf(out, "info: max strahler number = %d\n", max_strahler_number);
     fprintf(out, "info: number of index 1 bonds = %d\n", get_number_of_strahler_bonds(1));
     fprintf(out, "info: half system size = %g [m]\n", size_lateral / 2.0);
     fprintf(out, "info: system thickness = %g [m]\n", size_vertical);
     fprintf(out, "info: max dist from well to fracture element = %g [m]\n", max_dist1);
     fprintf(out, "info: max dist from well in grid = %g [m]\n", max_dist2);
     fprintf(out, "info: events = %d)\n", n_events);

     fprintf(out, "info: ---- effective stress -----\n");

     fprintf(out, "info: eff vertical stress at top = %.2f [MPa]\n", eff_sigma_zz_top / 1.0e+6);
     fprintf(out, "info: eff vertical stress at mid = %.2f [MPa]\n", eff_sigma_zz_mid / 1.0e+6);
     fprintf(out, "info: eff vertical stress at bot = %.2f [MPa]\n", eff_sigma_zz_bot / 1.0e+6);
     fprintf(out, "info: eff lithostatic at center  = %.2f [MPa]\n", eff_sigma_zz_ctr / 1.0e+6);

     fprintf(out, "info: eff min comp stress at top = %.2f [MPa]\n", eff_comp_top / 1.0e+6);
     fprintf(out, "info: eff min comp stress at mid = %.2f [MPa]\n", eff_comp_mid / 1.0e+6);
     fprintf(out, "info: eff min comp stress at bot = %.2f [MPa]\n", eff_comp_bot / 1.0e+6);

     fprintf(out, "info: ---- stress -----\n");

     fprintf(out, "info: vertical stress at top = %.2f [MPa]\n", sigma_zz_top / 1.0e+6);
     fprintf(out, "info: vertical stress at mid = %.2f [MPa]\n", sigma_zz_mid / 1.0e+6);
     fprintf(out, "info: vertical stress at bot = %.2f [MPa]\n", sigma_zz_bot / 1.0e+6);
     fprintf(out, "info: lithostatic at center  = %.2f [MPa]\n", sigma_zz_ctr / 1.0e+6);

     fprintf(out, "info: min comp stress at top = %.2f [MPa]\n", comp_top / 1.0e+6);
     fprintf(out, "info: min comp stress at mid = %.2f [MPa]\n", comp_mid / 1.0e+6);
     fprintf(out, "info: min comp stress at bot = %.2f [MPa]\n", comp_bot / 1.0e+6);

     fprintf(out, "info: ----\n");

     fprintf(out, "info: hydrostatic pressure at top = %.2f [MPa]\n", ph_top / 1.0e+6);
     fprintf(out, "info: hydrostatic pressure at mid = %.2f [MPa]\n", ph_mid / 1.0e+6);
     fprintf(out, "info: hydrostatic pressure at bot = %.2f [MPa]\n", ph_bot / 1.0e+6);

     fprintf(out, "info: ----\n");

     fprintf(out, "info: depth to top of system:  %.3f [m]\n", depth_top_surf);
     fprintf(out, "info: depth to base of system: %.3f [m]\n", depth_top_surf - size_vertical);

     fprintf(out, "info: system size: %g x %g x %g [m]\n",
          size_lateral, size_lateral, size_vertical);

     fprintf(out, "info: block size: dx=%.4f dy=%.4f dz=%.4f [m]\n",
          size_lateral  / grid1->n_x_elems, 
          size_lateral  / grid1->n_x_elems, 
          size_vertical / grid1->n_z_elems);

     fprintf(out, "info: grid: %d x %d x %d elements\n",
          grid1->n_x_elems, grid1->n_y_elems, grid1->n_z_elems);

     if (is_fxy_by_eff_stress)
     {
          fprintf(out, "info: f_prime_x = %g\n", factor_hh[0]);
          fprintf(out, "info: f_prime_y = %g\n", factor_hh[1]);
          fprintf(out, "info: f_prime_z = %g\n", factor_hh[2]);
     }
     else
     {
          fprintf(out, "info: f_prime_x = %g\n", get_primed_fac(factor_hh[0]));
          fprintf(out, "info: f_prime_y = %g\n", get_primed_fac(factor_hh[1]));
          fprintf(out, "info: f_prime_z = %g\n", get_primed_fac(factor_hh[2]));
     }

     if (is_fxy_by_eff_stress)
     {
          fprintf(out, "info: f_x = %g\n", get_unprimed_fac(factor_hh[0]));
          fprintf(out, "info: f_y = %g\n", get_unprimed_fac(factor_hh[1]));
          fprintf(out, "info: f_z = %g\n", get_unprimed_fac(factor_hh[2]));
     }
     else
     {
          fprintf(out, "info: f_x = %g\n", factor_hh[0]);
          fprintf(out, "info: f_y = %g\n", factor_hh[1]);
          fprintf(out, "info: f_z = %g\n", factor_hh[2]);
     }

     fprintf(out, "info: frac_scale_x = %g (bond strength)\n", frac_scale_dir[0]);
     fprintf(out, "info: frac_scale_y = %g (bond strength)\n", frac_scale_dir[1]);
     fprintf(out, "info: frac_scale_z = %g (bond strength)\n", frac_scale_dir[2]);

     fprintf(out, "info: a-param=%5.4f\n", get_a_param(sigma_zz_mid));
     fprintf(out, "info: b-param=%5.4f\n", get_b_param());

     fprintf(out, "info: well xpos = %.3f [m]\n", grid1->xcoord_elem[first_frac_elem]);
     fprintf(out, "info: well ypos = %.3f [m]\n", grid1->ycoord_elem[first_frac_elem]);
     fprintf(out, "info: well zpos = %.3f [m]\n", grid1->zcoord_elem[first_frac_elem]);
     fprintf(out, "info: total simulation time = %g [h]\n", t_end / 3600.0);
     fprintf(out, "info: lithostatic stress = %s\n", get_YES_or_NO(has_lithostatic));
     fprintf(out, "info: isotropic fracture scale = %s\n", get_YES_or_NO(is_isotropic_frac_scale));
     fprintf(out, "info: loopless = %s \n", get_YES_or_NO(is_loopless));

     fprintf(out, "info: x-bonds: weight=%5.3f, sum=%3d\n", bond_weight[0], bond_sum[0]);
     fprintf(out, "info: y-bonds: weight=%5.3f, sum=%3d\n", bond_weight[1], bond_sum[1]);
     fprintf(out, "info: z-bonds: weight=%5.3f, sum=%3d\n", bond_weight[2], bond_sum[2]);

     fprintf(out, "info: max overpres at base: %10.3e\n", get_max_overpres());
     fprintf(out, "info: first fracture pres:  %10.3e\n", pres_first_frac);
     fprintf(out, "info: number of chimneys:  %d\n", get_chimneys_reaching_surface(NULL));
     fprintf(out, "info: estimate of chimneys: %d\n", get_number_of_chimneys_estimate());
     fprintf(out, "info: estimate new chimney cells: %d\n", get_number_of_new_chimney_cells());
     fprintf(out, "info: bulk reservoir volume: %.1e [m3]\n", get_bulk_reservoir_vol());
     fprintf(out, "info: grid cell volume: %g [m3]\n", grid1->volume_elem[0]);
     fprintf(out, "info: source term: %.1e [1/s]\n", get_reservoir_rate_per_vol());
     fprintf(out, "info: time step: %g [s]\n", get_dt());
     

     if (out == stdout) print_separator(out);
}


static int *create_elem_to_point_map(
     FILE *out, 
     int size_list,
     int *list_bonds)
{
     int *map_elem_to_point_no = NULL;
     int i, k, elem, elem12[2];
     int n_mappings = 0;

     ABC_NEW_ARRAY_INIT(map_elem_to_point_no, int, grid1->n_elems, -1);

     for (i = 0; i < size_list; i++)
     {
          int bond = list_bonds[i];
          elem12[0] = grid1->bond_elem1[bond];
          elem12[1] = grid1->bond_elem2[bond];

          for (k = 0; k < 2; k++)
          {
               elem = elem12[k];

               if (map_elem_to_point_no[elem] < 0)
               {
                    map_elem_to_point_no[elem] = n_mappings;
                    n_mappings++;

                    fprintf(out, "%12.4e %12.4e %12.4e\n",
                         grid1->xcoord_elem[elem],
                         grid1->ycoord_elem[elem],
                         grid1->zcoord_elem[elem]);
               }
          }
     }

     return map_elem_to_point_no;
}


static void write_boundray_elems(
     void)
{
     const char *ext = "-bound.txt";
     FILE *out = abc_new_file(case_name, ext);
     int is_frac_boundary_elems = FALSE;
     int i, elem, is_x, is_y, is_z;
     double dist;

     if (first_frac_elem < 0) return;

     fprintf(out, "List of fractured boundary elements:");

     for (i = 0; i < n_frac_elems; i++)
     {
          elem = list_of_frac_elems[i];
          if (grid1->elem_info[elem] == 0) continue;
          is_x = (grid1->elem_info[elem] & (FEMBoundaryLeft | FEMBoundaryRight));
          is_y = (grid1->elem_info[elem] & (FEMBoundaryBack | FEMBoundaryFront));
          is_z = (grid1->elem_info[elem] & (FEMBoundaryTop | FEMBoundaryBot));
          dist = fem_get_elem_distance(grid1, first_frac_elem, elem);

          fprintf(out, "\nelem=%6d, dist=%7.2f, x=%7.2f, y=%7.2f, z=%7.2f, code=%c%c%c", 
               elem, dist,
               grid1->xcoord_elem[elem], 
               grid1->ycoord_elem[elem], 
               grid1->zcoord_elem[elem],
               (is_x ? 'X' : ' '), (is_y ? 'Y' : ' '), (is_z ? 'Z' : ' '));

          is_frac_boundary_elems = TRUE;
     }

     if (is_frac_boundary_elems)
          fprintf(out, "\n");
     else
          fprintf(out, " NONE\n");

     printf("Fractured boundary elements are written to: %s%s\n", case_name, ext);
}


static void write_animation(
     int counter,
     double t1)
{
     char cmd[ABC_MAX_WORD];
     const char *filename = "x1-anim.a2";

     if (not is_making_animation) return;

     write_results_to_file(filename);
     sprintf(cmd, "bash MakeAnim.sh %s %05d %g", filename, counter, t1);
     printf("(is running script: %s)\n", cmd);
     system(cmd);
}


static void write_info(
    void)
{
     const char *ext = "-info.txt";
     FILE *out = abc_new_file(case_name, ext); 
     show_info(out);
     printf("Info is written to: %s%s\n", case_name, ext);
     fclose(out);
}


static void write_results(
     void)
{
     char filename[ABC_MAX_WORD];
     sprintf(filename, "%s-res.a2", case_name);
     write_results_to_file(filename);
}


static void write_results_for_steps(
     int step)
{
     char filename[ABC_MAX_WORD];
     if (output_step_size < 1) return;
     if (step % output_step_size != 0) return;
     sprintf(filename, "%s-step-%d-res.a2", case_name, step);
     write_results_to_file(filename);
}


static void write_results_to_file(
     const char *file_name)
{
     AbcOutputStreamII stream;
     int n_nodes = grid1->n_nodes;
     int n_elems = grid1->n_elems;
     int n_bonds = grid1->n_bonds;

     abc_init_ascii_stream(&stream);
     abc_write_begin_II(&stream, file_name);
     write_params_II(&stream);
     fem_write_grid_part_II(&stream, grid1);
     abc_write_doubles_II(&stream, "xcoord", grid1->xcoord, n_nodes);
     abc_write_doubles_II(&stream, "ycoord", grid1->ycoord, n_nodes);
     abc_write_doubles_II(&stream, "zcoord", grid1->zcoord, n_nodes);
     abc_write_doubles_II(&stream, "xcoord_elem", grid1->xcoord_elem, n_elems);
     abc_write_doubles_II(&stream, "ycoord_elem", grid1->ycoord_elem, n_elems);
     abc_write_doubles_II(&stream, "zcoord_elem", grid1->zcoord_elem, n_elems);
     abc_write_doubles_II(&stream, "phi_elem", phi_elem, n_elems);
     abc_write_doubles_II(&stream, "pres_elem", pres_elem, n_elems);
     abc_write_doubles_II(&stream, "perm_bond", perm_bond, n_bonds);
     abc_write_doubles_II(&stream, "frac_limit_bond", frac_limit_bond, n_bonds);
     abc_write_doubles_II(&stream, "event_time1_elem", event_time1_elem, n_elems);
     abc_write_doubles_II(&stream, "event_time2_elem", event_time2_elem, n_elems);
     abc_write_doubles_II(&stream, "eff_lithostatic_elem", eff_lithostatic_elem, n_elems);
     abc_write_doubles_II(&stream, "eff_lithostatic_elem", eff_lithostatic_elem, n_elems);
     abc_write_ints_II(&stream, "is_fractured_bond", is_fractured_bond, n_bonds);
     abc_write_ints_II(&stream, "is_fractured_elem", is_fractured_elem, n_elems);
     abc_write_ints_II(&stream, "is_permeable_elem", is_permeable_elem, n_elems);
     abc_write_ints_II(&stream, "is_reservoir_elem", is_reservoir_elem, n_elems);
     abc_write_ints_II(&stream, "is_surface_chimney_elem", is_surface_chimney_elem, n_elems);
     abc_write_ints_II(&stream, "burst_elem", burst_elem, n_elems);
     abc_write_ints_II(&stream, "burst_size_elem", burst_size_elem, n_elems);
     abc_write_ints_II(&stream, "events_elem", events_elem, n_elems);
     abc_write_ints_II(&stream, "topo_bond", topo_bond, n_bonds);
     abc_write_ints_II(&stream, "shreve_bond", shreve_bond, n_bonds);
     abc_write_ints_II(&stream, "strahler_bond", strahler_bond, n_bonds);
     write_fracture_network_II(&stream);
     abc_write_end_II(&stream);
}


static void write_params_II(
     AbcOutputStreamII *stream)
{
     double dnx = (double) nl_nodes;
     double dny = (double) nl_nodes;
     double dnz = (double) nl_nodes;
     double dseed = (double) seed;
     double d_events = (double) n_events;
     double d_frac_elems = (double) n_frac_elems;
     double d_frac_bonds = (double) n_frac_bonds;
     double d_main_steps = (double) n_main_steps;

     abc_write_param_as_string_II(stream, "param_seed", "%g", dseed);
     abc_write_param_as_string_II(stream, "param_nx", "%g", dnx);
     abc_write_param_as_string_II(stream, "param_ny", "%g", dny);
     abc_write_param_as_string_II(stream, "param_nz", "%g", dnz);
     abc_write_param_as_string_II(stream, "param_lx", "%.3f (m)", size_lateral);
     abc_write_param_as_string_II(stream, "param_ly", "%.3f (m)", size_lateral);
     abc_write_param_as_string_II(stream, "param_lz", "%.3f (m)", size_vertical);
     abc_write_param_as_string_II(stream, "param_phi0", "%.4f (-)", phi0);
     abc_write_param_as_string_II(stream, "param_phi_res", "%.4f (-)", phi_res);
     abc_write_param_as_string_II(stream, "param_phi_cap", "%.4f (-)", phi_cap);
     abc_write_param_as_string_II(stream, "param_phi_dam", "%.4f (-)", phi_dam);
     abc_write_param_as_string_II(stream, "param_compres", "%.2e (1/Pa)", compres0);
     abc_write_param_as_string_II(stream, "param_perm_rock", "%.2e (m2)", perm_rock);
     abc_write_param_as_string_II(stream, "param_perm_frac_open", "%.2e (m2)", perm_frac_open);
     abc_write_param_as_string_II(stream, "param_perm_frac_closed", "%.2e (m2)", perm_frac_closed);
     abc_write_param_as_string_II(stream, "param_sigma_h", "%.2e (Pa)", sigma_h);
     abc_write_param_as_string_II(stream, "param_frac_scale", "%.2e (Pa)", frac_scale);
     abc_write_param_as_string_II(stream, "param_n_events", "%.0f (-)", d_events);
     abc_write_param_as_string_II(stream, "param_n_frac_elems", "%.0f (-)", d_frac_elems);
     abc_write_param_as_string_II(stream, "param_n_frac_bonds", "%.0f (-)", d_frac_bonds);
     abc_write_param_as_string_II(stream, "param_n_main_steps", "%.0f (-)", d_main_steps);
}


static void write_fracture_network_II(
     AbcOutputStreamII *stream)
{
     int i;
     int max_buf = 2 * n_frac_bonds;
     double *buffer1 = NULL;
     double *buffer2 = NULL;
     double *buffer3 = NULL;

     ABC_NEW_ARRAY(buffer1, double, max_buf);
     ABC_NEW_ARRAY(buffer2, double, max_buf);
     ABC_NEW_ARRAY(buffer3, double, max_buf);

     for (i = 0; i < n_frac_bonds; i++)
     {
          int i1 = 2 * i;
          int i2 = i1 + 1;
          int bond = list_of_frac_bonds[i];
          int elem1 = grid1->bond_elem1[bond];
          int elem2 = grid1->bond_elem2[bond];

          buffer1[i1] = grid1->xcoord_elem[elem1];
          buffer2[i1] = grid1->ycoord_elem[elem1];
          buffer3[i1] = grid1->zcoord_elem[elem1];

          buffer1[i2] = grid1->xcoord_elem[elem2];
          buffer2[i2] = grid1->ycoord_elem[elem2];
          buffer3[i2] = grid1->zcoord_elem[elem2];
     }    

     abc_write_doubles_II(stream, "x_frac_path", buffer1, max_buf);
     abc_write_doubles_II(stream, "y_frac_path", buffer2, max_buf);
     abc_write_doubles_II(stream, "z_frac_path", buffer3, max_buf);

     ABC_FREE_ARRAY(buffer1);
     ABC_FREE_ARRAY(buffer2);
     ABC_FREE_ARRAY(buffer3);
}


static void write_fracture_network_vtk1(
     int s0)
{
     int i;
     FILE *out = NULL;
     char ext[ABC_MAX_WORD] = "-network.vtk";
     int *map_elem_to_point_no = NULL;
     int n_mapping_errors = 0;

     if (s0 > 0) sprintf(ext, "-network-s%d.vtk", s0);

     out = abc_new_file(case_name, ext);

     fprintf(out, "# vtk DataFile Version 2.0\n");
     fprintf(out, "Full network\n");
     fprintf(out, "ASCII\n");
     fprintf(out, "DATASET POLYDATA\n");
     fprintf(out, "POINTS %d float\n", n_frac_elems);

     map_elem_to_point_no = create_elem_to_point_map(out, 
                                 n_frac_bonds, list_of_frac_bonds);

     fprintf(out, "LINES %d %d\n", n_frac_bonds, (3 * n_frac_bonds));

     for (i = 0; i < n_frac_bonds; i++)
     {
          int bond = list_of_frac_bonds[i];
          int elem1 = grid1->bond_elem1[bond];
          int elem2 = grid1->bond_elem2[bond];
          int n1 = map_elem_to_point_no[elem1];
          int n2 = map_elem_to_point_no[elem2];
          if (n1 < 0) n_mapping_errors++;
          if (n2 < 0) n_mapping_errors++;
          fprintf(out, "2 %d %d\n", n1, n2);
     }

     if (n_mapping_errors > 0)
          printf("ERROR: %d mapping errors in VTK-network file! (1)\n", n_mapping_errors);

     ABC_FREE_ARRAY(map_elem_to_point_no);

     fclose(out);
     printf("The fracture network is written to: %s%s\n", case_name, ext);
}


static void write_fracture_network_vtk2(
     void)
{
     int i;
     char ext[ABC_MAX_WORD] = "-network2.vtk";
     FILE *out = abc_new_file(case_name, ext);
     int *map_elem_to_point_no = NULL;
     int n_mapping_errors = 0;

     fprintf(out, "# vtk DataFile Version 2.0\n");
     fprintf(out, "Full network\n");
     fprintf(out, "ASCII\n");
     fprintf(out, "DATASET UNSTRUCTURED_GRID\n");
     fprintf(out, "POINTS %d float\n", n_frac_elems);

     map_elem_to_point_no = create_elem_to_point_map(out,
                                 n_frac_bonds, list_of_frac_bonds);

     fprintf(out, "CELLS %d %d\n", n_frac_bonds, (3 * n_frac_bonds));

     for (i = 0; i < n_frac_bonds; i++)
     {
          int bond = list_of_frac_bonds[i];
          int elem1 = grid1->bond_elem1[bond];
          int elem2 = grid1->bond_elem2[bond];
          int n1 = map_elem_to_point_no[elem1];
          int n2 = map_elem_to_point_no[elem2];
          if (n1 < 0) n_mapping_errors++;
          if (n2 < 0) n_mapping_errors++;
          fprintf(out, "2 %d %d\n", n1, n2);
     }

     if (n_mapping_errors > 0)
          printf("ERROR: %d mapping errors in VTK-network file! (2)\n", n_mapping_errors);

     fprintf(out, "CELL_TYPES %d\n", n_frac_bonds);

     for (i = 0; i < n_frac_bonds; i++)
          fprintf(out, "3\n");

     write_vtk_cell_data(out, n_frac_bonds);

     write_vtk_bond_data_ints(out, "dir_bond", dir_bond);
     write_vtk_bond_data_ints(out, "topo_bond", topo_bond);
     write_vtk_bond_data_ints(out, "shreve_bond", shreve_bond);
     write_vtk_bond_data_ints(out, "strahler_bond", strahler_bond);

     write_vtk_bond_data_doubles(out, "perm_bond", perm_bond);
     write_vtk_bond_data_doubles(out, "time_bond", time_bond);
     write_vtk_bond_data_doubles(out, "frac_limit_bond", frac_limit_bond);

     ABC_FREE_ARRAY(map_elem_to_point_no);

     fclose(out);
     printf("The fracture network2 is written to: %s%s\n", case_name, ext);
}


static void write_vtk_cell_data(
     FILE *out,
     int size)
{
     int i;

     fprintf(out, "CELL_DATA %d\n", size);
     fprintf(out, "SCALARS my_number int 1\n");
     fprintf(out, "LOOKUP_TABLE default\n");

     for (i = 0; i < size; i++)
          fprintf(out, "%d\n", i);
}


static void write_vtk_bond_data_ints(
     FILE *out, 
     const char *name,
     int *array)
{
     int i, b;

     fprintf(out, "FIELD FieldData 1\n");
     fprintf(out, "%s 1 %d float\n", name, n_frac_bonds);
     
     for (i = 0; i < n_frac_bonds; i++)
     {
          b = list_of_frac_bonds[i];
          fprintf(out, "%d\n", array[b]);
     }
}


static void write_vtk_bond_data_doubles(
     FILE *out, 
     const char *name, 
     double *array)
{    
     int i, b;

     fprintf(out, "FIELD FieldData 1\n");
     fprintf(out, "%s 1 %d float\n", name, n_frac_bonds);
     
     for (i = 0; i < n_frac_bonds; i++)
     {
          b = list_of_frac_bonds[i];
          fprintf(out, "%g\n", array[b]);
     }
}


static void write_longest_branch_vtk(
     void)
{
     int i, size2;
     const char *ext = "-branch.vtk";
     FILE *out = abc_new_file(case_name, ext);
     int *map_elem_to_point_no = NULL;
     int *list2_bond = NULL;
     int n_mapping_errors = 0;

     ABC_NEW_ARRAY(list2_bond, int, n_frac_bonds);
     find_longest_branch(list2_bond, &size2, n_frac_bonds);

     fprintf(out, "# vtk DataFile Version 2.0\n");
     fprintf(out, "Longest branch\n");
     fprintf(out, "ASCII\n");
     fprintf(out, "DATASET POLYDATA\n");
     fprintf(out, "POINTS %d float\n", n_frac_elems);

     map_elem_to_point_no = create_elem_to_point_map(out, size2, list2_bond);

     fprintf(out, "LINES %d %d\n", size2, 3 * size2);

     for (i = 0; i < size2; i++)
     {
          int bond = list2_bond[i];
          int elem1 = grid1->bond_elem1[bond];
          int elem2 = grid1->bond_elem2[bond];
          int n1 = map_elem_to_point_no[elem1];
          int n2 = map_elem_to_point_no[elem2];
          if (n1 < 0) n_mapping_errors++;
          if (n2 < 0) n_mapping_errors++;
          fprintf(out, "2 %d %d\n", n1, n2);
     }

     if (n_mapping_errors > 0)
          printf("ERROR: %d mapping errors in VTK-network file! (3)\n", n_mapping_errors);

     ABC_FREE_ARRAY(map_elem_to_point_no);
     ABC_FREE_ARRAY(list2_bond);

     fclose(out);
     printf("The longest branch is written to: %s%s\n", case_name, ext);
}


static void write_fracture_network_2d_PS(
     void)
{
     const char *ext = "-network.ps";
     double xmin =  1.0e+32, ymin =  1.0e+32;
     double xmax = -1.0e+32, ymax = -1.0e+32;
     double green_hls[3] = {180.0, 50.0, 100.0};
     int list_bond[MAX_LIST_SIZE], list_elem[MAX_LIST_SIZE];
     int i, xsteps, ysteps, size, size2, max_level;
     int *list2_bond = NULL; 
     double xc, yc;

     if (nv_nodes > 2) return;
     if (first_frac_elem < 0) return;

     xc = grid1->xcoord_elem[first_frac_elem];
     yc = grid1->ycoord_elem[first_frac_elem];

     abc_ps_begin(case_name, ext);
     abc_ps_set_font_size(12.0);

     for (i = 0; i < n_frac_elems; i++)
     {    
          int elem = list_of_frac_elems[i];
          double xx = grid1->xcoord_elem[elem];
          double yy = grid1->ycoord_elem[elem];

          if (xx < xmin) xmin = xx;
          if (xx > xmax) xmax = xx;
          if (yy < ymin) ymin = yy;
          if (yy > ymax) ymax = yy;
     }

     abc_nice_interval(&xmin, &xmax, &xsteps, 10);
     abc_nice_interval(&ymin, &ymax, &ysteps, 10);

     abc_ps_set_user_space(xmin, ymin, xmax, ymax);
     abc_ps_set_font_size(9.0);

     for (i = 0; i < n_frac_bonds; i++)
     {
          int bond = list_of_frac_bonds[i];
          write_bond_PS(bond, topo_bond[bond]);
     }

     max_level = get_max_topo_number();
     find_dead_end_bond_and_elem(max_level, list_bond, list_elem, &size, MAX_LIST_SIZE);

     for (i = 0; i < size; i++)
     {
          double xx = grid1->xcoord_elem[list_elem[i]];
          double yy = grid1->ycoord_elem[list_elem[i]];
          abc_ps_set_bullet_size(3.5);
          abc_ps_put_bullet(xx, yy);
     }

     abc_ps_set_bullet_size(4.5);
     abc_ps_put_bullet(xc, yc);

     ABC_NEW_ARRAY(list2_bond, int, n_frac_bonds);
     find_longest_branch(list2_bond, &size2, n_frac_bonds);
     abc_ps_set_line_width(4.0);
     abc_ps_set_line_color_hls(green_hls);

     for (i = 0; i < size2; i++) 
     {
          int bond = list2_bond[i];
          write_bond_PS(bond, -1);
     }

     ABC_FREE_ARRAY(list2_bond);
     abc_ps_end();

     printf("(PostScript test is written to: %s%s)\n", case_name, ext);
}


static void write_bond_PS(
     int bond,
     int level)
{
     char text[ABC_MAX_WORD];
     int elem1 = grid1->bond_elem1[bond];
     int elem2 = grid1->bond_elem2[bond];
     double x1 = grid1->xcoord_elem[elem1];
     double y1 = grid1->ycoord_elem[elem1];
     double x2 = grid1->xcoord_elem[elem2];
     double y2 = grid1->ycoord_elem[elem2];
     int s1 = strahler_bond[bond];
     int s2 = shreve_bond[bond];

     abc_ps_draw_line(x1, y1, x2, y2);

     if (level < 0) return;
     sprintf(text, "%d,%d", s1, s2);
     abc_ps_put_text(0.5 * (x1 + x2), 0.5 * (y1 + y2), text);
}


static void write_fracture_network_python(
     void)
{
     int i;
     const char *ext = "-network.py";
     FILE *out = abc_new_file(case_name, ext);

     fprintf(out, "import numpy as np\n");
     fprintf(out, "import matplotlib as mpl\n");
     fprintf(out, "import matplotlib.pyplot as plt\n");
     fprintf(out, "from mpl_toolkits.mplot3d import Axes3D\n");
     fprintf(out, "\n");

     fprintf(out, "fig = plt.figure()\n");
     fprintf(out, "ax = fig.gca(projection='3d')\n");
     fprintf(out, "col = 'b'\n");

     for (i = 0; i < n_frac_bonds; i++)
     {
          int bond = list_of_frac_bonds[i];
          int elem1 = grid1->bond_elem1[bond];
          int elem2 = grid1->bond_elem2[bond];

          double col = time_bond[bond] / t_end;

          double x1 = grid1->xcoord_elem[elem1];
          double y1 = grid1->ycoord_elem[elem1];
          double z1 = grid1->zcoord_elem[elem1];

          double x2 = grid1->xcoord_elem[elem2];
          double y2 = grid1->ycoord_elem[elem2];
          double z2 = grid1->zcoord_elem[elem2];

          fprintf(out, "ax.plot([%g, %g], [%g, %g], [%g, %g], c='%g')\n", 
               x1, x2, y1, y2, z1, z2, col);
     }

     fprintf(out, "ax1 = fig.add_axes([0.90, 0.10, 0.02, 0.80])\n");
     fprintf(out, "norm = mpl.colors.Normalize(vmin=0, vmax=%g)\n", t_end);
     fprintf(out, "cmap = mpl.cm.cool\n");
     fprintf(out, "cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap, norm=norm, orientation='vertical')\n");
     fprintf(out, "cb1.set_label('time (s)')\n");
     fprintf(out, "plt.show()\n");

     fclose(out);
     printf("Fracture network for matplotlib is written to: %s%s\n", case_name, ext);
}


static void write_to_log_file(
     int step,
     int broken_bonds,
     double fluid_pres)
{
     static FILE *out = NULL;

     if (out == NULL)
     {
          char filename[ABC_MAX_WORD];
          sprintf(filename, "%s-step.csv", case_name);
          out = abc_open_listed_file(filename, "Results for each step:", "w");

          fprintf(out, "#(\n");
          fprintf(out, "setvarname{buffer= 0, name=\"step\"}\n");
          fprintf(out, "setvarname{buffer= 1, name=\"broken_bonds\"}\n");
          fprintf(out, "setvarname{buffer= 2, name=\"fluid_pres\"}\n");
          fprintf(out, ")#\n");
     }

     fprintf(out, "%12d ", step);
     fprintf(out, "%12d ", broken_bonds);
     fprintf(out, "%12g ", fluid_pres);
     fprintf(out, "\n");
     fflush(out);
}


static void write_to_damage_log_file(
     int step,
     double t1)
{
     static FILE *out = NULL;
     double ph;

     if (first_frac_elem < 0) return;

     ph = dens_fluid * gravity * grid1->zcoord_elem[first_frac_elem];

     if (out == NULL)
     {
          char filename[ABC_MAX_WORD];
          sprintf(filename, "%s-time.csv", case_name);
          out = abc_open_listed_file(filename, "Results for each time-step:", "w");

          fprintf(out, "#(\n");
          fprintf(out, "setvarname{buffer= 0, name=\"step\"}\n");
          fprintf(out, "setvarname{buffer= 1, name=\"time_sec\"}\n");
          fprintf(out, "setvarname{buffer= 2, name=\"pres_fluid\"}\n");
          fprintf(out, "setvarname{buffer= 3, name=\"injected_vol\"}\n");
          fprintf(out, "setvarname{buffer= 4, name=\"total_events\"}\n");
          fprintf(out, "setvarname{buffer= 5, name=\"pres_approx\"}\n");
          fprintf(out, "setvarname{buffer= 6, name=\"pres_average\"}\n");
          fprintf(out, "setvarname{buffer= 7, name=\"compressed_vol_res\"}\n");
          fprintf(out, "setvarname{buffer= 8, name=\"pore_vol\"}\n");
          fprintf(out, "setvarname{buffer= 9, name=\"n_frac_cells\"}\n");
          fprintf(out, "setvarname{buffer=10, name=\"real_pres_fluid\"}\n");

          fprintf(out, "Element volume: %g [m3]\n", get_cell_volume());
          fprintf(out, "Element pore volume: %g [m3]\n", phi_res * get_cell_volume());
          fprintf(out, ")#\n");
     }

     fprintf(out, "%12d ", step);
     fprintf(out, "%12g ", t1);
     fprintf(out, "%12g ", pres_elem[first_frac_elem]);
     fprintf(out, "%12g ", get_injected_vol());
     fprintf(out, "%12d ", n_events);
     fprintf(out, "%12g ", 0.0);

     fprintf(out, "%12g ", get_average_res_pres());
     fprintf(out, "%12g ", get_compressed_vol_in_res());
     fprintf(out, "%12g ", get_total_pore_vol());
     fprintf(out, "%12d ", n_frac_elems);
     fprintf(out, "%12g ", ph + pres_elem[first_frac_elem]);
     fprintf(out, "\n");
     fflush(out);
}


static void write_all_events_to_file(
     void)
{
     int i;
     const char *ext = "-events.csv";
     FILE *out = abc_new_file(case_name, ext);
     double x0, y0, z0;

     if (first_frac_elem < 0) return;

     x0 = grid1->xcoord_elem[first_frac_elem];
     y0 = grid1->ycoord_elem[first_frac_elem];
     z0 = grid1->zcoord_elem[first_frac_elem];

     fprintf(out, "#(\n");
     fprintf(out, "setvarname{buffer=0, name=\"index\"}\n");
     fprintf(out, "setvarname{buffer=1, name=\"broken_bonds\"}\n");
     fprintf(out, "setvarname{buffer=2, name=\"event_xpos\"}\n");
     fprintf(out, "setvarname{buffer=3, name=\"event_ypos\"}\n");
     fprintf(out, "setvarname{buffer=4, name=\"event_zpos\"}\n");
     fprintf(out, "setvarname{buffer=5, name=\"event_radius\"}\n");
     fprintf(out, "setvarname{buffer=6, name=\"event_time1\"}\n");
     fprintf(out, "setvarname{buffer=7, name=\"event_time2\"}\n");
     fprintf(out, "setvarname{buffer=8, name=\"magnitude\"}\n");
     fprintf(out, ")#\n");

     for (i = 0; i < n_events; i++)
     {
          double xpos = get_mean_over_event_elems(i, grid1->xcoord_elem);
          double ypos = get_mean_over_event_elems(i, grid1->ycoord_elem);
          double zpos = get_mean_over_event_elems(i, grid1->zcoord_elem);
          double dx = xpos - x0;
          double dy = ypos - y0;
          double dz = zpos - z0;
          double radius = sqrt((dx * dx) + (dy * dy) + (dz * dz));
          double M = log10((double) list_of_event_size[i]);

          fprintf(out, "%d %d %g %g %g %g %g %g %g\n",
               i, list_of_event_size[i],
               xpos, ypos, zpos, radius,
               event_time1[i], event_time2[i], M);
     }

     fclose(out);
     printf("Total number of events are written to: %s%s\n", case_name, ext);
}


static void write_ricther_data(
     void)
{
     const char *ext = "-richter.csv";
     FILE *out = abc_new_file(case_name, ext);
     double dS = 4.0 / (n_bins - 1.0);
     double epsilon = 0.001;
     double S[MAX_BINS], N[MAX_BINS];
     double log10S[MAX_BINS], log10N[MAX_BINS];
     double M, log10NA, log10NB;
     int i, k;

     fprintf(out, "#( N = Magnitude (number of broken bonds).\n");
     fprintf(out, "setvarname{offset=0, buffer=0, name=\"log10S\"}\n");
     fprintf(out, "setvarname{offset=0, buffer=1, name=\"log10N\"}\n");
     fprintf(out, "setvarname{offset=0, buffer=2, name=\"N\"}\n");
     fprintf(out, ")#\n");

     /* Make the bins. */

     for (i = 0; i < n_bins; i++)
     {
          S[i] = pow(10.0, i * dS);
          N[i] = 0.0;
     }

     /* Loop over the events and fill the bins. */

     for (k = 0; k < n_events; k++)
     {
          M = list_of_event_size[k];
 
          for (i = 0; i < n_bins; i++)
               if (M > S[i]) 
                    N[i] += 1;
     }

     /* Make log10 of the bin values. */

     for (i = 0; i < n_bins; i++)
     {
          log10S[i] = log10(S[i]);
          log10N[i] = 0.0;
          
          if (N[i] > 0.0) 
               log10N[i] = log10(N[i]);
     }

     /* Write out. */

     log10NA = log10N[0];
     log10NB = log10N[n_bins - 1];

     for (i = 1; i < n_bins - 1; i++)
     {
          if (ABC_ABS(log10N[i+1] - log10NA) < epsilon) continue;
          if (ABC_ABS(log10N[i-1] - log10NB) < epsilon) continue;
          fprintf(out, "%10.4f ", log10S[i]);
          fprintf(out, "%10.4f ", log10N[i]);
          fprintf(out, "%10.4f ", N[i]);
          fprintf(out, "\n");
     }

     fclose(out);
     printf("Event size distribution is written to: %s%s\n", case_name, ext);
}


static void write_elements_in_event(
     void)
{
     const char *ext = "-event.a2";
     FILE *out = abc_new_file(case_name, ext);
     int i, k, size;

     fprintf(out, "begin_results2\n");

     for (i = 0; i < n_events; i++)
     {
          size = get_elems_in_event(i, buffer_of_elem, grid1->n_elems);

          fprintf(out, "int event%-5d %3d  ", i, size);
          for (k = 0; k < size; k++)
               fprintf(out, " %d", buffer_of_elem[k]);
          fprintf(out, "\n");
     }
     
     fprintf(out, "end_results2\n");

     fclose(out);
     printf("The elements in the events are written to: %s%s\n", case_name, ext);
}


static void write_damage_per_elem_row(
     void)
{
     const char *ext = "-per-row.csv";
     FILE *out = abc_new_file(case_name, ext);
     int n_z_elems = grid1->n_z_elems;
     int n_h_elems = fem_get_n_h_elems(grid1);
     int sum1 = 0, sum2 = 0;
     int i, irel, elem, elem1, elem2, row_inj;
     double fraction;

     if (first_frac_elem < 0) return;

     row_inj = first_frac_elem / n_h_elems;

     for (i = 0; i < n_z_elems; i++)
     {
          sum1 = 0;
          elem1 = i * n_h_elems;
          elem2 = elem1 + n_h_elems;
          irel = i - row_inj;
          
          for (elem = elem1; elem < elem2; elem++)
               if (is_fractured_elem[elem]) sum1 += 1;

          sum2 += sum1;
          fraction = ((double) sum1) / ((double) n_frac_elems);

          fprintf(out, "%3d %10.3f %3d %10.5f %2d\n", 
               i, grid1->zcoord[elem1], sum1, fraction, irel);
     }

     if (n_frac_elems != sum2)
          printf("Error: Expected n_frac_elems=%d, but got %d\n", n_frac_elems, sum2);

     fclose(out);
     printf("Broken cells per row is written to: %s%s\n", case_name, ext);
}


static void make_burst_size_elem(
     void)
{
     int i, k, elem, size, value;

     for (i = 0; i < n_events; i++)
     {
          size = get_elems_in_event(i, buffer_of_elem, grid1->n_elems);

          if      (size > 10) value = 3;
          else if (size >  5) value = 2;
          else                value = 1;

          for (k = 0; k < size; k++)
          {
               elem = buffer_of_elem[k];
               burst_size_elem[elem] = value;
               events_elem[elem] = i;
               event_time1_elem[elem] = event_time1[i];
               event_time2_elem[elem] = event_time2[i];
          }
     }
}


static double get_random_number(
     void)
{
     double xx = nr_ran1(&seed);
     /***
     double numb = pow(xx, nn_exp) / (nn_exp + 1.0);
     return numb;
     ***/
     return xx;
}


static double get_mean_over_event_elems(
     int i,
     double *array_of_elem)
{
     double counter = 0.0;
     double mean = 0.0;
     double sum = 0.0;
     int elem;
     
     for (elem = first_elem_in_burst[i]; 
          elem >= 0; 
          elem = next_elem_in_burst[elem])
     {
          sum += array_of_elem[elem];
          counter += 1.0;
     }

     if (counter > 0)
          mean = sum / counter;

     return mean;
}


static int get_number_of_strahler_bonds(
     int s0)
{
     int i;
     int counter = 0;

     if (s0 < 1) return n_frac_bonds;

     for (i = 0; i < n_frac_bonds; i++)
     {
          int bond = list_of_frac_bonds[i];
          if (strahler_bond[bond] == s0) counter++;
     }

     return counter;
}


static int get_elems_in_event(
     int i,
     int *elem_in_event,
     int max_array)
{
     int elem;
     int size = 0;

     for (elem = first_elem_in_burst[i];
          elem >= 0;
          elem = next_elem_in_burst[elem])
     {
          if (size >= max_array)
               ABC_RETURN_FALSE("[get_elems_in_event] Too small array!");

          elem_in_event[size] = elem;
          size++;
     }

     return size;
}


static double get_min_factor_hh(
     void)
{
     double x1 = ABC_MIN(factor_hh[0], factor_hh[1]);
     double x2 = ABC_MIN(factor_hh[2], x1);
     return x2;
}


static double get_unprimed_fac(
     double fprime)
{
     double fac = fprime + (1.0 - fprime) * (dens_fluid / dens_bulk);
     return fac;
}


static double get_primed_fac(
     double fac)
{
     double ratio = dens_fluid / dens_bulk;
     double primed_fac = (fac - ratio) / (1.0 - ratio);
     return primed_fac;
}


static double get_a_param(
     double sigma_v)
{
     double fx, fy, a_param;
     double mx = frac_scale_dir[0];

     if (is_fxy_by_eff_stress)
     {
          fx = get_unprimed_fac(factor_hh[0]);
          fy = get_unprimed_fac(factor_hh[1]);
     }
     else
     {
          fx = factor_hh[0];
          fy = factor_hh[1];
     }

     a_param = (fy - fx) * sigma_v / mx;
     return a_param;
}


static double get_b_param(
     void)
{
     double mx = frac_scale_dir[0];
     double mz = frac_scale_dir[2];
     double b_param = (mz - mx) / mx;
     return b_param;
}


static double get_depth_of_bond(
     int b)
{
     double *depth_elem = grid1->zcoord_elem;
     int elem1 = grid1->bond_elem1[b];
     int elem2 = grid1->bond_elem2[b];

     if (elem1 < 0 and elem2 < 0) ABC_ERROR_EXIT("Unconnected bond!");
     if (elem1 < 0) return depth_elem[elem2];
     if (elem2 < 0) return depth_elem[elem1];

     return 0.5 * (depth_elem[elem1] + depth_elem[elem2]);
}


static double get_depth_top_reservoir(
     double x,
     double y)
{
     double two_pi = 2.0 * ABC_PI;
     double pi_half = 0.5 * ABC_PI;
     double x1 = x + 0.5 * size_lateral;
     double y1 = y + 0.5 * size_lateral;
     double Lx = size_lateral / n_xdomes;
     double Ly = size_lateral / n_ydomes;
     double zpos = 0.0;
     double sinx = sin((two_pi * x1 / Lx) - pi_half);
     double siny = sin((two_pi * y1 / Ly) - pi_half);
     double z0 = - size_vertical;

     zpos = (1.0 + sinx * siny) * dome_A0 + res_h0 + z0;

     return zpos;
}


static int get_chimneys_reaching_surface(
     int *list)
{
     int n, elem;
     int counter = 0;

     for (n = 0; n < n_frac_elems; n++)
     {
          elem = list_of_frac_elems[n];
          if (not is_close_to_surface_elem(elem)) continue;
          if (list != NULL) list[counter] = elem;
          counter++;
     }

     return counter;
}


static int get_number_of_chimneys_estimate(
     void)
{
     double Vb = get_bulk_reservoir_vol();
     double l0 = size_vertical - dome_A0 - dome_A0 - res_h0;
     double dx = size_lateral / (nl_nodes - 1.0);
     double dA = dx * dx;
     double df = pfac2 - pfac1;
     double n0 = compres0 * phi_res * visc0 * l0 * df * Vb / (t_end * perm_frac_closed * dA);

     /***
     printf("Vb=%g, l0=%g, dA=%g, df=%g, alpha=%g, phi=%g, visc=%g, kf=%g, n0=%g)\n",
          Vb, l0, dA, df, compres0, phi_res, visc0, perm_frac_closed, n0);
     ***/

     int n1 = (int) n0;
     return n1;
}


static int get_number_of_new_chimney_cells(
     void)
{
     double dt = get_dt();
     double S0 = get_reservoir_rate_per_vol();
     double Vb = get_bulk_reservoir_vol();
     double dV = grid1->volume_elem[0];
     double n0 = compres0 * Vb * S0 * dt / dV;
     int n1 = (int) n0;
     return n1;
}


static void find_bond_xyz_pos(
     int b,
     double *x1,
     double *y1,
     double *z1)
{
     int elem1 = grid1->bond_elem1[b];
     int elem2 = grid1->bond_elem2[b];
     double *xcoord_elem = grid1->xcoord_elem;
     double *ycoord_elem = grid1->ycoord_elem;
     double *zcoord_elem = grid1->zcoord_elem;

     if (elem1 < 0 and elem2 < 0)
          ABC_ERROR_EXIT("[find_bond_xyz_pos] Illegal bond!");

     if (elem1 < 0)
     {
          *x1 = xcoord_elem[elem2];
          *y1 = ycoord_elem[elem2];
          *z1 = zcoord_elem[elem2];

          return;
     }

     if (elem2 < 0)
     {
          *x1 = xcoord_elem[elem1];
          *y1 = ycoord_elem[elem1];
          *z1 = zcoord_elem[elem1];

          return;
     }

     *x1 = 0.5 * (xcoord_elem[elem1] + xcoord_elem[elem2]);
     *y1 = 0.5 * (ycoord_elem[elem1] + ycoord_elem[elem2]);
     *z1 = 0.5 * (zcoord_elem[elem1] + zcoord_elem[elem2]);

     return;
}

