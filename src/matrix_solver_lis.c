/*                                                                                         
**   This program is free software under GNU General Public License.                       
**   (c) Copyright 2015-2015 by Magnus Wangen
**
**   File: matrix_solver_lis.c
**   Title: An interface to the LIS-library
**   Date: Version 1.0, January 2015
**   Author: Magnus Wangen (Magnus.Wangen@gmail.com)
**
**   $Id$
*/

/*
**    \chapter{GNU General Public License}
**
**   This program is free software; you can redistribute it and/or
**   modify it under the terms of the GNU General Public License as
**   published by the Free Software Foundation; either version 2 of
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
*/

/*
**   Notes
**   =====
**
**   Install: 
**        sudo sh ./configure --enable-omp --enable-gprof CFLAGS=-g
**        sudo make
**        sudo make install
** 
**   The include file is here:
**        /usr/local/include/lis.h
**        $HOME/lis-1.5.57$ ls include
** 
**   The library is found at these two places: 
**        /usr/local/lib/liblis.a                 # after install
**        $HOME/lis-1.5.57/src/.libs/liblis.a     # after make
** 
**        NOTICE the directory name ".libs" !!!   # Notice the dot!
*/ 

#include <lis.h>
#include <matrix.h>
#include <matrix_solver_lis.h>

typedef struct _LISVars_ LISVars;

struct _LISVars_ {
     LIS_MATRIX A2;
     LIS_VECTOR b2;
     LIS_VECTOR x2;
     LIS_VECTOR u2;
     LIS_SOLVER solver;
};

static void give_user_info_and_exit(char **argv);
static void install_lis_solver_plain(void);
static void install_lis_solver_cg_ilu(void);
static void install_lis_solver_bicg_ilu(void);
static void install_lis_solver_gmres_ilu(void);
static LISVars *create_lis_vars(void);
static void delete_lis_vars(void **pp);
static void lis_check_solution(LIS_MATRIX A2, LIS_VECTOR b2, LIS_VECTOR x2);
static void make_lis_matrix(AbcMatrix *Aabc);
static void write_lis_system_to_file(AbcMatrix *A1);

static int
     is_verbose = TRUE,
     is_debugging = FALSE;

static char
     lis_string[ABC_MAX_WORD] = "";


#ifdef _ABC_MAIN_LIS
int main(
#else
int abc_demo_lis(
#endif
     int argc, 
     char **argv)
{
     AbcMatrix *A;
     AbcVector *b, *x, *r, *Ax;
     double epsilon;
     int is_sym;

     const char *filename_A = "xtest.mat";
     const char *filename_b = "xtest.vec";

     lis_initialize(&argc, &argv);

     if (argc < 3) give_user_info_and_exit(argv);

     filename_A = argv[1];
     filename_b = argv[2];

     /*
     ** Read A-matrix and b-matrix from file.
     */

     A = abc_sparse_matrix_read_ij_and_create_from_file(filename_A);
     if (A == 0) ABC_ERROR_EXIT("Can't continue!");

     b = abc_vector_read_and_create_from_file(filename_b);
     if (b == 0) ABC_ERROR_EXIT("Can't continue!");

     is_sym = abc_sparse_matrix_is_symmetric(A);

     printf("(A matrix is %s and has rows=%d, cols=%d and non-zeros=%d)\n", 
          (is_sym) ? "symmetric" : "non-symmetric", A->rows, A->cols, A->non_zeros);
     printf("(b vector has rows=%d)\n", b->size);

     /*
     ** Allocate space.
     */

     x = abc_vector_create(b->size, "solution");
     r = abc_vector_create(b->size, "difference: Ax - b");
     Ax = abc_vector_create(b->size, "product: Ax");

     abc_vector_zero(x);
     abc_vector_zero(r);
     abc_vector_zero(Ax);

     /*
     ** Solve the system.
     */

     abc_vector_zero(x);
     abc_solver_lis_plain(A, b, x);
     abc_store_vector_to_file(x, "x-plain.vec");
     A->solver->is_factorized = FALSE;

     abc_vector_zero(x);
     abc_solver_lis_cg_ilu(A, b, x);
     abc_store_vector_to_file(x, "x-cg-ilu.vec");
     A->solver->is_factorized = FALSE;

     abc_vector_zero(x);
     abc_solver_lis_bicg_ilu(A, b, x);
     abc_store_vector_to_file(x, "x-bicg-ilu.vec");
     A->solver->is_factorized = FALSE;

     abc_vector_zero(x);
     abc_solver_lis_gmres_ilu(A, b, x);
     abc_store_vector_to_file(x, "x-gmres-ilu.vec");
     A->solver->is_factorized = FALSE;

     /*
     ** Write the system to file using lis-functions.
     */

     write_lis_system_to_file(A);

     /*
     ** Check solution.
     */

     abc_matrix_mul_vector(A, x, Ax);
     abc_vector_sub_vector(Ax, b, r);
     epsilon = abc_vector_norm_l2(r);
     printf("l2norm(Ax-b)=%10.4e (from ABC)\n", epsilon);

     abc_matrix_delete(&A);
     abc_vector_delete(&b);
     abc_vector_delete(&x);
     abc_vector_delete(&r);
     abc_vector_delete(&Ax);

     abc_delete_input_lib();
     lis_finalize();

     return 0;
}


static void give_user_info_and_exit(
     char **argv)
{
     printf("Usage: %s <input-matrix> <input-vector>\n", argv[0]);
     printf("Ex: %s AA.mat bb.vec\n", argv[0]);
     exit(0);
}


void abc_install_lis_solver(
     int argc,
     char **argv)
{
     install_lis_solver_plain();
     install_lis_solver_cg_ilu();
     install_lis_solver_bicg_ilu();
     install_lis_solver_gmres_ilu();

     lis_initialize(&argc, &argv);
}


static void install_lis_solver_plain(
     void)
{
     AbcSolverInfo *solver = abc_solver_get_unused_solver_info();

     solver->matrix_type = AbcMatrixTypeSparse;
     solver->is_iterative = TRUE;
     solver->solver = abc_solver_lis_plain;
     strcpy(solver->option, "-solverLIS1");
     strcpy(solver->keyword, "LIS1");
     strcpy(solver->full_name, "LIS from Library of Iterative Solvers");
     printf("(\"%s\" is added to the list of solver options)\n", solver->keyword);
}


static void install_lis_solver_cg_ilu(
     void)
{
     AbcSolverInfo *solver = abc_solver_get_unused_solver_info();

     solver->matrix_type = AbcMatrixTypeSparse;
     solver->is_iterative = TRUE;
     solver->solver = abc_solver_lis_cg_ilu;
     strcpy(solver->option, "-solver-LIS-CG-ILU");
     strcpy(solver->keyword, "LIS-CG-ILU");
     strcpy(solver->full_name, "LIS-CG-ILU from Library of Iterative Solvers");
     printf("(\"%s\" is added to the list of solver options)\n", solver->keyword);
}


static void install_lis_solver_bicg_ilu(
     void)
{
     AbcSolverInfo *solver = abc_solver_get_unused_solver_info();

     solver->matrix_type = AbcMatrixTypeSparse;
     solver->is_iterative = TRUE;
     solver->solver = abc_solver_lis_bicg_ilu;
     strcpy(solver->option, "-solver-LIS-BiCG-ILU");
     strcpy(solver->keyword, "LIS-BiCG-ILU");
     strcpy(solver->full_name, "LIS-BiCG-ILU from Library of Iterative Solvers");
     printf("(\"%s\" is added to the list of solver options)\n", solver->keyword);
}


static void install_lis_solver_gmres_ilu(
     void)
{
     AbcSolverInfo *solver = abc_solver_get_unused_solver_info();

     solver->matrix_type = AbcMatrixTypeSparse;
     solver->is_iterative = TRUE;
     solver->solver = abc_solver_lis_gmres_ilu;
     strcpy(solver->option, "-solver-LIS-GMRES-ILU");
     strcpy(solver->keyword, "LIS-GMREM-ILU");
     strcpy(solver->full_name, "LIS-GMREM-ILU from Library of Iterative Solvers");
     printf("(\"%s\" is added to the list of solver options)\n", solver->keyword);
}


void abc_free_lis_memory(
     void)
{
     lis_finalize();
}


void abc_solver_lis_plain(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     strcpy(lis_string, "-i bicg -p none");
     abc_solver_lis(A1, b1, x1);
}


void abc_solver_lis_cg_ilu(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     strcpy(lis_string, "-i cg -p ilu");
     abc_solver_lis(A1, b1, x1);
}


void abc_solver_lis_bicg_ilu(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     strcpy(lis_string, "-i bicg -p ilu");
     abc_solver_lis(A1, b1, x1);
}


void abc_solver_lis_gmres_ilu(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     strcpy(lis_string, "-i gmres -p ilu");
     abc_solver_lis(A1, b1, x1);
}


void abc_solver_lis(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     LISVars *vars;
     AbcLinEqSolver *sol = A1->solver;
     char text1[ABC_MAX_WORD], text2[ABC_MAX_WORD];
     char solver_name[ABC_MAX_WORD], precon_name[ABC_MAX_WORD];
     double *bx, *xx, residual;
     int ns, np, iters, time_used, time_total, t0 = 0, t1 = 0;
     size_t size = (size_t) b1->size;
     size_t i;

     if (sol == NULL)
          ABC_ERROR_RETURN("[abc_solver_lis] solver==NULL!");

     abc_start_clock(sol->clock);

     /* 
     ** The flag |is_factorized=0| when the matrix is rebuild. 
     */

     if ((sol->extra == NULL) or (not sol->is_factorized))
     {
          t0 = abc_get_time_in_sec();
          make_lis_matrix(A1);
          t1 = abc_get_time_in_sec();
     }

     vars = (LISVars *) A1->solver->extra;

     bx = vars->b2->value;
     for (i = 0; i < size; i++)
          bx[i] = b1->value[i];

     /* 
     ** Get lis solver info.
     */

     lis_solver_get_solver(vars->solver, &ns);
     lis_solver_get_solvername(ns, solver_name);
     lis_solver_get_precon(vars->solver, &np);
     lis_solver_get_preconname(np, precon_name);

     /* 
     ** Find solution.
     */

     if (is_verbose) 
          printf("(lis: solver \"%s\" and preconditioning \"%s\")\n",
               solver_name, precon_name);

     lis_solve(vars->A2, vars->b2, vars->x2, vars->solver);
     
     /* 
     ** Unpack the solution.
     */

     xx = vars->x2->value;
     for (i = 0; i < size; i++)
          x1->value[i] = xx[i];

     /*
     ** Check the solution.
     */

     if (is_debugging) 
          lis_check_solution(vars->A2, vars->b2, vars->x2);

     /*
     ** Do reporting.
     */

     abc_stop_clock(A1->solver->clock);
     abc_read_clock(A1->solver->clock, &time_used, &time_total);
     lis_solver_get_iter(vars->solver, &iters);
     lis_solver_get_residualnorm(vars->solver, &residual);

     sprintf(text1, "[abc_solver_lis_%s] (precond: %s)", 
          solver_name, precon_name);
     sprintf(text2, " n=%3d (N=%6d) res=%10.4e, used=%ds total=%ds, realloc=%ds\n",
          iters, A1->rows, residual, time_used, time_total, t1 - t0);

     abc_matrix_report(text1, text2);

     /* At this point the matrix is always factorized. */

     sol->is_factorized = TRUE;
}


static LISVars *create_lis_vars(
     void)
{
     LISVars *vars;

     ABC_NEW_OBJECT(vars, LISVars);

     vars->A2 = NULL;
     vars->b2 = NULL;
     vars->x2 = NULL;
     vars->u2 = NULL;
     vars->solver = NULL;

     return vars;
}


static void delete_lis_vars(
     void **pp)
{
     LISVars *vars;

     if (pp == NULL) return;
     vars = (LISVars *) (*pp);
     if (vars == NULL) return;

     lis_solver_destroy(vars->solver);
     lis_matrix_destroy(vars->A2);
     lis_vector_destroy(vars->b2);
     lis_vector_destroy(vars->x2);
     lis_vector_destroy(vars->u2);

     ABC_FREE(vars);
     *pp = NULL;
}


static void lis_check_solution(
     LIS_MATRIX A2,
     LIS_VECTOR b2,
     LIS_VECTOR x2)
{
     LIS_VECTOR bx, rx;
     LIS_SCALAR norm2;

     lis_vector_duplicate(x2, &bx);
     lis_vector_duplicate(x2, &rx);

     lis_matvec(A2, x2, bx);
     lis_vector_axpyz(-1.0, b2, bx, rx);
     lis_vector_nrm2(rx, &norm2);

     lis_vector_destroy(bx);
     lis_vector_destroy(rx);

     printf("norm2(b-Ax) %8.1e (from lis)\n", (double) norm2);
}


static void make_lis_matrix(
     AbcMatrix *Aabc)
{
     int i, n;
     int nnz = Aabc->non_zeros;
     AbcLinEqSolver *sol = Aabc->solver;
     LISVars *vars;

     LIS_INT gn = (LIS_INT) Aabc->rows;
     LIS_INT k, nn, is, ie, err;
     LIS_INT *ptr, *index;
     LIS_SCALAR *value;

     vars = (LISVars *) sol->extra;
     if (vars != NULL) delete_lis_vars(&sol->extra);
     vars = create_lis_vars();
     sol->extra = (void *) vars;
     sol->delete_extra = delete_lis_vars;

     /***
     if (abc_matrix_is_symmetric(Aabc))
          printf("[make_lis_matrix] Symmetric matrix!\n");
     ***/

     err = lis_matrix_create(LIS_COMM_WORLD, &vars->A2);         CHKERR(err);
     err = lis_matrix_set_size(vars->A2, 0, gn);                 CHKERR(err);
     err = lis_matrix_get_size(vars->A2, &nn, &gn);              CHKERR(err);
     err = lis_matrix_malloc_csr(nn, nnz, &ptr, &index, &value); CHKERR(err);
     err = lis_matrix_get_range(vars->A2, &is, &ie);             CHKERR(err);

     k = 0;

     for (i = 0; i < Aabc->rows; i++)
     {
          ptr[i] = k;

          for (n = Aabc->adr[i]; n < Aabc->adr[i+1]; n++)
          {
               index[k] = Aabc->col[n];
               value[k] = Aabc->buffer[n];
               k++;
          }
     }

     ptr[i] = k;

     if (k != (LIS_INT) nnz)
          ABC_ERROR_EXIT("[make_lis_matrix] Loop error!");

     err = lis_matrix_set_csr(ptr[ie-is], ptr, index, value, vars->A2);  CHKERR(err);
     err = lis_matrix_assemble(vars->A2);                        CHKERR(err);

     err = lis_vector_duplicate(vars->A2, &vars->u2);            CHKERR(err);
     err = lis_vector_duplicate(vars->u2, &vars->b2);            CHKERR(err);
     err = lis_vector_duplicate(vars->u2, &vars->x2);            CHKERR(err);

     err = lis_solver_create(&vars->solver);                     CHKERR(err);
     err = lis_solver_set_optionC(vars->solver);                 CHKERR(err);
     err = lis_solver_set_option(lis_string, vars->solver);      CHKERR(err);
     /***
     lis_solver_set_option("-print mem", vars->solver);
     ***/
}


static void write_lis_system_to_file(
     AbcMatrix *A1)
{     
     LISVars *vars = (LISVars *) A1->solver->extra;

     if (vars == 0) ABC_ERROR_EXIT("[write_lis_system_to_file] NULL-pointer!");
     lis_output_matrix(vars->A2, LIS_FMT_MM, (char *) "x-lis-A2.mm");
     lis_output_vector(vars->b2, LIS_FMT_MM, (char *) "x-lis-b2.mm");
     printf("(has written lis-system to: x-lis-A2.mm to x-lis-b2.mm)\n");
}

