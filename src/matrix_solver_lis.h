#ifndef _MATRIX_SOLVER_LIS_H_
#define _MATRIX_SOLVER_LIS_H_

int  abc_demo_lis(int argc, char **argv);
void abc_install_lis_solver(int argc, char **argv);
void abc_free_lis_memory(void);
void abc_solver_lis_plain(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);
void abc_solver_lis_cg_ilu(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);
void abc_solver_lis_bicg_ilu(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);
void abc_solver_lis_gmres_ilu(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);
void abc_solver_lis(AbcMatrix *A1, AbcVector *b1, AbcVector *x1);

#endif
