/*                                                                                         
**   This program is free software under GNU General Public License.                       
**   (c) Copyright 2015-2015 by Magnus Wangen
**
**   File: matrix_solver_cholmod.c
**   Title: An interface to the CholMod-library
**   Date: Version 1.0, January 2015
**   Author: Magnus Wangen (Magnus.Wangen@gmail.com)
**
**   $Id$
*/

/*
**    \chapter{GNU General Public License}
**
**   This program is free software; you can redistribute it and/or
**   modify it under the terms of the GNU General Public License as
**   published by the Free Software Foundation; either version 2 of
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
*/

#include <cholmod.h>
#include <matrix.h>
//#include <matrix_solver_cholmod.h>

typedef struct _CholmodVars_ CholmodVars;

struct _CholmodVars_ {
     cholmod_sparse *A2;
     cholmod_factor *L2;
     cholmod_dense  *x2;
     cholmod_dense  *b2;
     cholmod_common  c;
};


static void give_user_info_and_exit(char **argv);
static CholmodVars *create_cholmod_vars(void);
static void delete_cholmod_vars(void **pp);
static void cholmod_check_solution(cholmod_sparse *A2, 
     cholmod_dense *b2, cholmod_dense *x2, cholmod_common *c);
static cholmod_sparse *abc_matrix_to_cholmod(AbcMatrix *Aabc, cholmod_common *c);
static void cholmod_write_sparse_to_file(cholmod_sparse *A, 
     cholmod_common *c, const char *filename);

static int
     is_verbose = FALSE,
     is_checking = FALSE,
     is_debugging_A2 = FALSE;


#ifdef _ABC_MAIN_CHOLMOD
int main(int argc, char **argv);

int main(int argc, char **argv)
{
     abc_demo_cholmod(argc, argv);
     abc_delete_input_lib();

     return 0;
}
#endif


void abc_install_cholmod_solver(
     void)
{
     AbcSolverInfo *solver = abc_solver_get_unused_solver_info();

     solver->matrix_type = AbcMatrixTypeSparse;
     solver->is_iterative = TRUE;
     solver->solver = abc_solver_cholmod;
     strcpy(solver->option, "-solverCholmod");
     strcpy(solver->keyword, "Cholmod");
     strcpy(solver->full_name, "Cholmod direct solver from SuiteSparse");

     printf("(\"%s\" is added to the list of solvers)\n", solver->keyword);
}


void abc_demo_cholmod(
     int argc, 
     char **argv)
{
     AbcMatrix *A;
     AbcVector *b, *x, *r, *Ax;
     double epsilon;
     int i, is_sym;

     const char *filename_A = "xtest.mat";
     const char *filename_b = "xtest.vec";
     const char *filename_x = "xx.vec";

     if (argc == 2 and ABC_MATCH(argv[1], "-h")) give_user_info_and_exit(argv);
     if (argc == 2 and ABC_MATCH(argv[1], "-help")) give_user_info_and_exit(argv);

     if (argc >= 2) filename_A = argv[1];
     if (argc >= 3) filename_b = argv[2];
     if (argc >= 4) filename_x = argv[3];

     /*
     ** Read A-matrix and b-matrix from file.
     */

     A = abc_sparse_matrix_read_ij_and_create_from_file(filename_A);
     if (A == 0) ABC_ERROR_EXIT("Can't continue!");

     b = abc_vector_read_and_create_from_file(filename_b);
     if (b == 0) ABC_ERROR_EXIT("Can't continue!");

     is_sym = abc_sparse_matrix_is_symmetric(A);

     printf("(A matrix is %s and has rows=%d, cols=%d and non-zeros=%d)\n", 
          (is_sym) ? "symmetric" : "non-symmetric", 
          A->rows, A->cols, A->non_zeros);
     printf("(b vector has rows=%d)\n", b->size);

     /*
     ** Allocate space.
     */

     x = abc_vector_create(b->size, "solution");
     r = abc_vector_create(b->size, "difference: Ax - b");
     Ax = abc_vector_create(b->size, "product: Ax");

     abc_vector_zero(x);
     abc_vector_zero(r);
     abc_vector_zero(Ax);

     /*
     ** Solve the system.
     */

     for (i = 0; i < 5; i++)
          abc_solver_cholmod(A, b, x);

     abc_store_vector_to_file(x, filename_x);

     /*
     ** Check solution.
     */

     abc_matrix_mul_vector(A, x, Ax);
     abc_vector_sub_vector(Ax, b, r);
     epsilon = abc_vector_norm_l2(r);
     printf("l2norm(Ax-b)=%10.4e (from ABC)\n", epsilon);

     abc_matrix_delete(&A);
     abc_vector_delete(&b);
     abc_vector_delete(&x);
     abc_vector_delete(&r);
     abc_vector_delete(&Ax);
}


static void give_user_info_and_exit(
     char **argv)
{
     printf("usage: <input-matrix> <input-vector> <output-solution>\n");
     printf("default: %s xtest.mat xtest.vec xx.sol\n", argv[0]);
     exit(0);
}


void abc_solver_cholmod(
     AbcMatrix *A1,
     AbcVector *b1,
     AbcVector *x1)
{
     CholmodVars *vars;
     AbcLinEqSolver *sol = A1->solver;
     char text[ABC_MAX_WORD];
     int t0, t1, t2, t3, time_used, time_total;
     double *bx, *xx;
     size_t i;

     if (sol == NULL)
          ABC_ERROR_RETURN("[abc_solver_cholmod] solver==NULL!");

     abc_start_clock(sol->clock);
     vars = (CholmodVars *) sol->extra;

     if (not sol->is_factorized)
     {
          printf("(cholmod: new factorization)\n");
          if (sol->extra != NULL) 
               delete_cholmod_vars(&sol->extra);

          sol->delete_extra = delete_cholmod_vars;
          sol->extra = (void *) create_cholmod_vars();

          vars = (CholmodVars *) sol->extra;
          vars->A2 = abc_matrix_to_cholmod(A1, &vars->c);
          vars->b2 = cholmod_ones(vars->A2->nrow, 1, vars->A2->xtype, &vars->c);
     }

     if (is_debugging_A2)
     {
          cholmod_print_sparse(vars->A2, "A", &vars->c);
          cholmod_write_sparse_to_file(vars->A2, &vars->c, "xlib.mat");
     }

     bx = vars->b2->x;
     for (i = 0; i < vars->b2->nrow; i++)
          bx[i] = b1->value[i];

     /* 
     ** Find solution: Analyse, factorize and solve.
     */

     t0 = abc_get_time_in_sec();

     if (not sol->is_factorized)
     {
          if (is_verbose) printf("(cholmod: analyse)\n");
          vars->L2 = cholmod_analyze(vars->A2, &vars->c);
          t1 = abc_get_time_in_sec();

          if (is_verbose) printf("(cholmod: factorize)\n");
          cholmod_factorize(vars->A2, vars->L2, &vars->c);
          t2 = abc_get_time_in_sec();
     }
     else
     {
          t1 = abc_get_time_in_sec();
          t2 = abc_get_time_in_sec();
     }


     if (is_verbose) printf("(cholmod: solve)\n");
     if (vars->x2 != NULL) cholmod_free_dense(&vars->x2, &vars->c);
     vars->x2 = cholmod_solve(CHOLMOD_A, vars->L2, vars->b2, &vars->c);
     t3 = abc_get_time_in_sec();
     
     /* 
     ** Unpack the solution.
     */

     xx = vars->x2->x;
     for (i = 0; i < vars->x2->nrow; i++)
          x1->value[i] = xx[i];

     /*
     ** Check the solution.
     */

     if (is_checking) 
          cholmod_check_solution(vars->A2, vars->b2, vars->x2, &vars->c);

     /*
     ** Free working space.
     */

     abc_stop_clock(A1->solver->clock);
     abc_read_clock(A1->solver->clock, &time_used, &time_total);

     if (sol->is_factorized)
          sprintf(text, " N=%d used=%ds total=%ds (analyse=no, factorize=no, solve=%ds)\n", 
               A1->rows, time_used, time_total, t3 - t2);
     else
          sprintf(text, " N=%d used=%ds total=%ds (analyse=%ds, factorize=%ds, solve=%ds)\n",
               A1->rows, time_used, time_total, t1 - t0, t2 - t1, t3 - t2);

     abc_matrix_report("[abc_solver_cholmod]", text);

     /* At this point the matrix is always factorized. */

     sol->is_factorized = TRUE;
}


static CholmodVars *create_cholmod_vars(
     void)
{
     CholmodVars *vars;

     ABC_NEW_OBJECT(vars, CholmodVars);

     vars->L2 = NULL;
     vars->A2 = NULL;
     vars->x2 = NULL;
     vars->b2 = NULL;

     cholmod_start(&vars->c);   /* start CHOLMOD */
     vars->c.print = 5;         /* enable printing */

     return vars;
}


static void delete_cholmod_vars(
     void **pp)
{
     CholmodVars *vars;

     if (pp == NULL) return;
     vars = (CholmodVars *) (*pp);
     if (vars == NULL) return;

     cholmod_free_factor(&vars->L2, &vars->c);
     cholmod_free_sparse(&vars->A2, &vars->c);
     cholmod_free_dense(&vars->x2, &vars->c);
     cholmod_free_dense(&vars->b2, &vars->c);
     cholmod_finish(&vars->c);

     ABC_FREE(vars);
     *pp = NULL;
}


static void cholmod_check_solution(
     cholmod_sparse *A2,
     cholmod_dense *b2,
     cholmod_dense *x2,
     cholmod_common *c)
{
     cholmod_dense *r2;
     double one[2] = {1.0, 0.0};
     double m1[2] = {-1.0, 0.0};                   /* basic scalars */
     double epsilon;

     r2 = cholmod_copy_dense(b2, c);               /* r = b */
     cholmod_sdmult(A2, 0, m1, one, x2, r2, c);    /* r = r-Ax */
     epsilon = cholmod_norm_dense(r2, 0, c);
     printf("norm(b-Ax) %8.1e (from cholmod)\n", epsilon);
     cholmod_free_dense(&r2, c);
}


static cholmod_sparse *abc_matrix_to_cholmod(
     AbcMatrix *Aabc,
     cholmod_common *c)
{
     int i, n;
     int dim = Aabc->rows;
     int nnz = Aabc->non_zeros;
     int *ii, *jj;
     double *xx;
     cholmod_sparse *A2; 
     cholmod_triplet *A1;
     size_t k = 0;

     /***
     if (not abc_matrix_is_symmetric(Aabc))
          printf("[abc_matrix_to_cholmod] Not symmetric matrix!\n");
     ***/

     A1 = cholmod_allocate_triplet(dim, dim, nnz, 0, CHOLMOD_REAL, c);
     ii = A1->i;
     jj = A1->j;
     xx = A1->x;

     for (i = 0; i < Aabc->rows; i++)
     {
          for (n = Aabc->adr[i]; n < Aabc->adr[i+1]; n++)
          {
               ii[k] = i;
               jj[k] = Aabc->col[n];
               xx[k] = Aabc->buffer[n];
               k++;
          }
     }

     if (k != (size_t) nnz)
          ABC_ERROR_EXIT("[abc_matrix_to_cholmod] Loop error!");

     A1->nnz = k;
     A2 = cholmod_triplet_to_sparse(A1, nnz, c);
     A2->stype = -1;    /* = lower half of sym matrix */

     cholmod_free_triplet(&A1, c);

     return A2;
}


static void cholmod_write_sparse_to_file(
     cholmod_sparse *A,
     cholmod_common *c,
     const char *filename)
{     
     int print_saved = c->print;
     FILE *out = fopen(filename, "w");

     c->print = 5;
     cholmod_write_sparse(out, A, NULL, "", c);
     fclose(out);
     c->print = print_saved;
     printf("(sparse cholmod-matrix is written to: %s)\n", filename);
}

