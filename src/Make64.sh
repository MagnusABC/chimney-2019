#
# Directories
# ===========
#

ABC=$HOME/abc
SUITSPARSE=$HOME/SuiteSparse

#
# Include files
# =============
#

INC1=$ABC/include
INC2=$SUITSPARSE/SuiteSparse_config
INC3=$SUITSPARSE/CHOLMOD/Include
INC4=/usr/local/include

INCLUDE="-I$INC1 -I$INC2 -I$INC3 -I$INC4 -I."

#
# Libraries
# =========
#

LIB11=$ABC/libABC.a

LIB21=$SUITSPARSE/CHOLMOD/Lib/libcholmod.a
LIB22=$SUITSPARSE/SuiteSparse_config/libsuitesparseconfig.a
LIB23=$SUITSPARSE/AMD/Lib/libamd.a          
LIB24=$SUITSPARSE/COLAMD/Lib/libcolamd.a      

LIB31=/usr/local/lib/liblis.a

#LIB41=$ABC/linux/clapack/lapack.a
#LIB42=$ABC/linux/clapack/blas.a
#LIB43=$ABC/linux/clapack/libf2c.a

LIBRARIES="$LIB11 $LIB21 $LIB22 $LIB23 $LIB24 $LIB31 -lblas -lm -lrt -llapack"

#
# The -W-flags to the compiler enable very useful warnings.
# =========================================================
#

FLAGS="-fopenmp -g -DABC_PRAGMA -DABC_CHOLMOD -DABC_LIS_SOLVER"
FLAGS="$FLAGS -Wall -W -Wpointer-arith -Waggregate-return"
FLAGS="$FLAGS -Wstrict-prototypes -Wnested-externs"
FLAGS="$FLAGS -Wmissing-declarations -Wmissing-prototypes"
FLAGS="$FLAGS -Wwrite-strings -Wredundant-decls"

#
# Compile and loading
# ===================
#

MORE_C_CODE="fvm_network_solver.c matrix_solver_cholmod.c matrix_solver_lis.c"

for PROGRAM in print_time damage3d-ns-chimney; do
    gcc $INCLUDE $FLAGS ${PROGRAM}.c $MORE_C_CODE -o $PROGRAM $LIBRARIES
    echo "(has complied and loaded: $PROGRAM)"
done



