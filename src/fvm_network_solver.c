/*
**   This program is free software under GNU General Public License.
**   (c) Copyright 1989-2004 by Magnus Wangen
**
**   File: fvm_network_solver.c
**   Title: A FVM network solver
**   Date: Version 1.0, January 2004
**   Author: Magnus Wangen (Magnus.Wangen@gmail.com)
**
**   $Id$
*/

/*
**    \chapter{GNU General Public License}
**
**   This program is free software; you can redistribute it and/or
**   modify it under the terms of the GNU General Public License as
**   published by the Free Software Foundation; either version 2 of
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
*/


/*
**   Introduction
**
**   This file has data structures and functions for solving a 
**   scalar parabolic PDE on a network defined 
**   by connected elements on a regular grid.
**   The network solver is implemented as ABC-library functions.
**   Input is a |pde| and an array |is_network_elem| marking the elements. 
**   Output is the solution mapped back to the |pde|.
**   The |pde| needs to compute element and bond properties on
**   the mapped grid (the network grid).
**   See the example below for how the network solver can be used.
*/

#include <fem.h>
#include "fvm_network_solver.h"

static AbcParams *def_params(void);
static int   *get_marked_elems(FEMGrid *grid);
static int    get_center_element_2d(FEMGrid *grid);
static void   make_bond_coefs(FEMDiffEq *pde, int b, FEMBondCoefs *coef);
static void   make_elem_coefs(FEMDiffEq *pde, FEMType *fem, FEMCoefs *coef);
static int    is_center_elem(int elem1, int *elem2);
static double get_bond_perm(int b);
static double get_stationary_pres(FEMNetworkSolver *ns);
static double get_transient_pres(FEMNetworkSolver *ns, double t);


static int 
     nn = 24,
     nx_step = -1,
     ny_step = -1,
     center_elem = -1,
     is_debugging = FALSE,
     is_transient_model = FALSE,
     do_full_solution = TRUE,
     show_all_bonds = FALSE,
     show_main_bonds = FALSE;

static int
     *is_marked_elem = NULL;

static double 
     t_end = 1.0,
     phi0 = 1.0,
     compres0 = 1.0,
     perm_frac = 1.0,
     perm_rock = 1.0e-6,
     system_length = 1.0,
     injection_rate = 1.0;

static FEMNetworkSolver
     *current_ns = NULL;


#ifdef FVM_NS_TEST
int main(
#else
int fvm_network_solver_demo(
#endif
     int argc,
     char **argv)
{
     int BC1 = FEMBC1stKind;
     int counter = 0;
     AbcParams *params = NULL;
     FEMGrid *grid = NULL;
     FEMDiffEq *pde = NULL;
     FEMNetworkSolver *ns = NULL;
     double t0, dt;

#ifdef ABC_CHOLMOD
     abc_install_cholmod_solver();
#else
     printf("(info: Cholmod is not installed!)\n");
#endif

     params = def_params();
     abc_read_all_params_by_args(params, argc, argv);

     /*
     ** (1) Make the main solver.
     */

     printf("PART I: Full grid.\n");

     grid = fem_create_regular_grid(AbcGridQuadLinear, nn, nn, nn);
     fem_translate_grid(grid, -0.5, -0.5, 0.0);
     fem_scale_grid(grid, system_length, system_length, 1.0);
     fem_make_volume_and_center_coords(grid);

     center_elem = get_center_element_2d(grid);
     printf("volume center elem: %g\n", grid->volume_elem[center_elem]);

     pde = fvm_create_bond_pde(grid, make_elem_coefs, 
          make_bond_coefs, AbcMatrixTypeSparse);

     fem_add_boundary_condition(pde, BC1, FEMBoundaryRight, 0, fem_zero_value);
     fem_add_boundary_condition(pde, BC1, FEMBoundaryLeft,  0, fem_zero_value);
     fem_add_boundary_condition(pde, BC1, FEMBoundaryBot,   0, fem_zero_value);
     fem_add_boundary_condition(pde, BC1, FEMBoundaryTop,   0, fem_zero_value);

     if (do_full_solution)
     {
          fvm_basic_solver(pde);
          fem_store_solution(pde, "x1.a2");
     }

     /*
     ** (2) Make a network.
     */

     is_marked_elem = get_marked_elems(grid);

     /*
     ** (3) Create a network solver.
     */

     printf("PART II: Network solver.\n");

     ns = fvm_create_network_solver(pde, is_marked_elem, 
          make_elem_coefs, make_bond_coefs, AbcMatrixTypeSparse);

     fvm_check_all_bonds(ns);
     fvm_set_current_network_solver(ns);

     if (show_all_bonds)
          fvm_print_network_all_bonds(ns);

     if (show_main_bonds)
          fvm_print_network_main_bonds(ns);

     /*
     ** (4) Stationary solution with network solver.
     */

     is_transient_model = FALSE;
     fvm_set_solution_to_zero(pde);
     fvm_network_solve(ns, 0.0, -1.0);
     fem_store_solution(ns->pde_new, "x2.a2");
     fem_store_solution(pde, "x3.a2");
     /***
     fvm_print_network_bonds_and_elems(ns);
     ***/

     printf("NUMERICAL stationary pressure in center: %g\n", 
          ns->pde_ptr->solution1->value[center_elem]);
     printf("APPROXIMATE stationary pressure: %g\n", 
          get_stationary_pres(ns));

     /*
     ** (5) Time stepping with network solver.
     */

     is_transient_model = TRUE;
     fvm_set_solution_to_zero(pde);

     t0 = 0.0;
     dt = t_end / 10.000001;

     for (t0 = 0.0; t0 + dt < t_end; t0 += dt)
     {
          fvm_network_solve(ns, t0, t0+dt);

          printf("Time step %3d from: %5.3g to %5.3g, NUMERICAL=%10.3g, APPROX=%10.3g\n", 
               counter, t0, t0 + dt, 
               ns->pde_ptr->solution1->value[center_elem],
               get_transient_pres(ns, t0 + dt));

          counter++;
     }

     fem_store_solution(pde, "x4.a2");

     /*
     ** (6) Clean up.
     */

     fvm_delete_network_solver(&ns);
     fem_delete_grid(&grid);
     abc_delete_params(&params);
     ABC_DELETE_LIB()

     ABC_UNUSED_PARAMETER(argc);
     ABC_UNUSED_PARAMETER(argv);

     return 0;
}



static AbcParams *def_params(
     void)
{
     int GR1 = 1;
     AbcParams *params = abc_new_params(1024);

     abc_use_params_input_block(params, TRUE);
     abc_def_param_group(params, "Parameters:", GR1);

     abc_def_int_param(params, &nn, "nn", "-",
          "Number nodes x- and y-directions", 24, 1, 1024, GR1);
     abc_def_int_param(params, &nx_step, "sx", "-",
          "Number of elements in init frac x-dir", -1, -1, 1024, GR1);
     abc_def_int_param(params, &ny_step, "sy", "-",
          "Number of elements in init frac y-dir", -1, -1, 1024, GR1);
     abc_def_double_param(params, &system_length, "len", "m",
          "Length in x- and y-directions", 1.0, 1.0, 1.0e+9, GR1);
     abc_def_double_param(params, &phi0, "phi", "-",
          "Porosity", 1.0, 0.001, 1.0, GR1);
     abc_def_double_param(params, &compres0, "compres", "1/Pa",
          "Compressibility", 1.0, 1.0e-15, 1.0, GR1);
     abc_def_double_param(params, &injection_rate, "rate", "m3/s",
          "Injection rate", 1.0, 1.0e-15, 1.0+9, GR1);
     abc_def_double_param(params, &perm_frac, "kf", "m2",
          "Permeability fracture", 1.0, 1.0e-19, 1.0e+2, GR1);
     abc_def_double_param(params, &perm_rock, "kr", "m2",
          "Permeability rock", 1.0e-6, 1.0e-23, 1.0e+2, GR1);
     abc_def_double_param(params, &t_end, "tend", "s",
          "Time end-of-simulation", 1.0, 1.0e-9, 1.0e+15, GR1);
     abc_def_boolean_param(params, &is_debugging, "debug",
          "Enable/disable debugging mode", TRUE, GR1);
     abc_def_boolean_param(params, &do_full_solution, "full",
          "Enable/disable full solution", TRUE, GR1);
     abc_def_boolean_param(params, &show_all_bonds, "show-all",
          "Enable/disable printing of all bonds", FALSE, GR1);
     abc_def_boolean_param(params, &show_all_bonds, "show-main",
          "Enable/disable printing of main bonds", FALSE, GR1);

     return params;
}


static int *get_marked_elems(
     FEMGrid *grid)
{
     int n_h_elems = grid->n_x_elems;
     int xsize = nx_step;
     int ysize = ny_step;
     int *is_target_elem= NULL;
     int counter = 0;
     int i, elem;

     if (xsize < 0) {xsize = (int) (0.2 * (double) grid->n_x_elems);}
     if (ysize < 0) {ysize = (int) (0.2 * (double) grid->n_y_elems);}

     is_target_elem = abc_alloc_ints_once(grid->n_elems);
     abc_init_ints(is_target_elem, grid->n_elems, FALSE);

     for (i = 0; i < xsize; i++)
     {
          is_target_elem[center_elem + i] = TRUE;
          is_target_elem[center_elem - i] = TRUE;
     }

     for (i = 0; i < ysize; i++)
     {
          is_target_elem[center_elem + i * n_h_elems] = TRUE;
          is_target_elem[center_elem - i * n_h_elems] = TRUE;
     }

     for (elem = 0; elem < grid->n_elems; elem++)
          if (is_target_elem[elem])
          {
               printf("%3d ", elem); 
               counter++;
          }

     printf("\n");
     

     printf("(number of elements in network: %d)\n", counter);

     return is_target_elem;
}


static int get_center_element_2d(
     FEMGrid *grid)
{
     int col = (grid->n_x_nodes - 2) / 2;
     int row = (grid->n_y_nodes - 2) / 2;
     int elem = (row * grid->n_x_elems) + col; 

     printf("center elem: %d, pos: (%.3f, %.3f)\n", elem,
          grid->xcoord_elem[elem], grid->ycoord_elem[elem]);

     return elem;
}


static void make_bond_coefs(
     FEMDiffEq *pde,
     int b,
     FEMBondCoefs *coef)
{
     coef->diffusivity = get_bond_perm(b);
     coef->have_velocity_term = FALSE;
     coef->velocity = 0.0;

     ABC_UNUSED_PARAMETER(pde);
}


static void make_elem_coefs(
     FEMDiffEq *pde,
     FEMType *fem,
     FEMCoefs *coef)
{
     int elem = fem->element;
     int elem2 = -1;
     double *vol_elem = pde->grid->volume_elem;

     /* Part 1: Storage */

     coef->have_storage_term = is_transient_model;
     coef->storage = phi0 *compres0;

     /* Part 2: Diffusivity */

     coef->have_velocity_term = FALSE;

     coef->diffusivity[0][0] = 1.0;
     coef->diffusivity[1][1] = 1.0;
     coef->diffusivity[2][2] = 1.0;

     /* Part 3: Source */

     coef->have_source_term = FALSE;                                                      
     coef->source = 0.0;                                                                  
                                                                                          
     /***
     if (elem != center_elem) return;                                                           
     ***/

     if (not is_center_elem(elem, &elem2)) return;

     coef->have_source_term = TRUE;
     coef->source = injection_rate / vol_elem[elem2];
} 


static int is_center_elem(
     int elem1,
     int *elem2)
{
     FEMNetworkSolver *ns = fvm_get_current_network_solver();

     if (ns == 0)
          *elem2 = elem1;
     else
          *elem2 = ns->map_elem_new_to_old[elem1];

     return (*elem2 == center_elem);
}


static double get_bond_perm(
     int b)
{
     FEMNetworkSolver *ns = fvm_get_current_network_solver();
     int e1, e2, elem1, elem2;
     double perm = perm_frac;

     if (ns == 0) return perm;

     e1 = ns->grid_new->bond_elem1[b];
     e2 = ns->grid_new->bond_elem2[b];

     if (e1 > e2) abc_swap_ints(&e1, &e2);

     elem1 = ns->map_elem_new_to_old[e1];
     elem2 = ns->map_elem_new_to_old[e2];

     if (elem1 > elem2) abc_swap_ints(&elem1, &elem2);

     if (not ns->is_network_elem[elem1]) perm = perm_rock;
     if (not ns->is_network_elem[elem2]) perm = perm_rock;

     return perm;
}


static double get_stationary_pres(
     FEMNetworkSolver *ns)
{
     double fact = fvm_get_leakage_factor(ns);
     double pres = fact * injection_rate / perm_rock;
     return pres;
}


static double get_transient_pres(
     FEMNetworkSolver *ns,
     double t)
{
     double vol2 = phi0 * fvm_get_volume_factor(ns);
     double vol1 = injection_rate * t;
     double pres = vol1 / (vol2 * compres0);
     return pres;
}

/*
**   ===============================
**   The library routines are below.
**   ===============================
*/

void fvm_set_current_network_solver(
     FEMNetworkSolver *ns)
{
     current_ns = ns;
}


FEMNetworkSolver *fvm_get_current_network_solver(
     void)
{
     return current_ns;
}


FEMNetworkSolver *fvm_create_network_solver(
     FEMDiffEq *pde,
     int *is_network_elem,
     FEM_MAKE_ELEM_COEFS make_elem_coefs,
     FEM_MAKE_BOND_COEFS make_bond_coefs,
     int matrix_type)
{
     int BC1 = FEMBC1stKind;
     FEMNetworkSolver *ns = NULL;
     FEMGrid *grid = pde->grid;
     int n_elems = grid->n_elems;
     int n_bonds = grid->n_bonds;

     ABC_NEW_OBJECT(ns, FEMNetworkSolver);

     ns->is_verbose = TRUE;
     ns->pde_ptr = pde;
     ns->grid_old = fem_copy_grid(grid);
     ns->grid_new = fem_copy_grid(grid);
     ns->is_network_elem = is_network_elem;

     ABC_NEW_ARRAY_INIT(ns->map_elem_old_to_new, int, n_elems, -1);
     ABC_NEW_ARRAY_INIT(ns->map_elem_new_to_old, int, n_elems, -1);
     ABC_NEW_ARRAY_INIT(ns->map_bond_old_to_new, int, n_bonds, -1);
     ABC_NEW_ARRAY_INIT(ns->map_bond_new_to_old, int, n_bonds, -1);

     fvm_make_maps_for_network_solver(ns);

     ns->pde_new = fvm_create_scalar_bond_pde(ns->grid_new, 
          make_elem_coefs, make_bond_coefs, matrix_type);

     fem_add_boundary_condition(ns->pde_new, BC1, 
          FEMBoundaryNS, 0, fem_zero_value);

     fvm_set_current_network_solver(ns);

     return ns;
}


FEMDiffEq *fvm_create_scalar_bond_pde(
     FEMGrid *grid,
     FEM_MAKE_ELEM_COEFS elem_coefs,
     FEM_MAKE_BOND_COEFS bond_coefs,
     int matrix_type)
{
     AbcMatrix *A;
     FEMDiffEq *pde;
     int n_scalar_unknowns = grid->n_elems;

     A = fvm_create_stiff_matrix(grid, matrix_type);
     pde = fem_create_pde_syst_basic(grid, 1, n_scalar_unknowns, A);
     pde->make_elem_stiff = NULL;
     pde->make_stiff_and_load = fvm_make_stiff_and_load;
     pde->solve_lin_eq_system = fem_solve_lin_eq_system;
     pde->is_using_bonds = TRUE;
     pde->solve = fvm_basic_solver;
     pde->galerkin_method = FEM_FINITE_VOLUME_METHOD;
     pde->make_elem_coefs = elem_coefs;
     pde->make_bond_coefs = bond_coefs;

     return pde;
}


void fvm_delete_network_solver(
     FEMNetworkSolver **pp)
{
     FEMNetworkSolver *ns;

     if (pp == NULL) return;
     ns = *pp;
     if (ns == NULL) return;

     if (ns->is_verbose)
          printf("(network solver is cleaning up)\n");

     fem_delete_grid(&ns->grid_old);
     fem_delete_grid(&ns->grid_new);

     ABC_FREE(ns->map_elem_old_to_new);
     ABC_FREE(ns->map_elem_new_to_old);
     ABC_FREE(ns->map_bond_old_to_new);
     ABC_FREE(ns->map_bond_new_to_old);

     *pp = NULL;
}


void fvm_network_solve(
     FEMNetworkSolver *ns,
     double t0,
     double t1)
{
     FEMDiffEq *pde_old = ns->pde_ptr;
     FEMDiffEq *pde_new = ns->pde_new;
     int type = AbcMatrixTypeSparse;
     int elem_new, elem_old, n_new_elems;

     fvm_make_maps_for_network_solver(ns);
     n_new_elems = ns->grid_new->n_elems;

     abc_vector_delete(&ns->pde_new->solution0);
     abc_vector_delete(&ns->pde_new->solution1);
     abc_vector_delete(&ns->pde_new->solution2);
     abc_vector_delete(&ns->pde_new->load);
     abc_matrix_delete(&ns->pde_new->stiff);

     ns->pde_new->solution0 = abc_vector_create(n_new_elems, "sol0");
     ns->pde_new->solution1 = abc_vector_create(n_new_elems, "sol1");
     ns->pde_new->solution2 = abc_vector_create(n_new_elems, "sol2");
     ns->pde_new->load      = abc_vector_create(n_new_elems, "load");
     ns->pde_new->stiff     = fvm_create_stiff_matrix(ns->pde_new->grid, type);

     abc_solver_set(ns->pde_new->stiff, "-dummy -solverCholmod");

     /* Update the time in the old pde. */

     pde_old->t0 = t0;
     pde_old->t1 = t1;

     for (elem_new = 0 ; elem_new < n_new_elems; elem_new++)
     {
          elem_old = ns->map_elem_new_to_old[elem_new];
          pde_new->solution0->value[elem_new] = pde_old->solution0->value[elem_old];
          pde_new->solution1->value[elem_new] = pde_old->solution1->value[elem_old];
          pde_new->solution2->value[elem_new] = pde_old->solution2->value[elem_old];
     }

     if (t1 < t0)
     {
          if (ns->is_verbose) 
               printf("(making stationary network solution)\n");

          fvm_basic_solver(pde_new);
     }
     else
     {
          if (ns->is_verbose) 
          {
               printf("(doing network time step: ");
               abc_print_time(stdout, t0);
               printf(" to ");
               abc_print_time(stdout, t1);
               printf(")\n");
          }

          fvm_do_time_step(pde_new, t0, t1);
     }

     for (elem_new = 0 ; elem_new < n_new_elems; elem_new++)
     {
          elem_old = ns->map_elem_new_to_old[elem_new];
          pde_old->solution0->value[elem_old] = pde_new->solution0->value[elem_new];
          pde_old->solution1->value[elem_old] = pde_new->solution1->value[elem_new];
          pde_old->solution2->value[elem_old] = pde_new->solution2->value[elem_new];
     }
}


void fvm_make_maps_for_network_solver(
     FEMNetworkSolver *ns)
{
     int b, elem1, elem2;
     int n_elems = ns->grid_old->n_elems;
     int n_bonds = ns->grid_old->n_bonds;
     int *is_elem = ns->is_network_elem; 

     if (ns->is_verbose)
          printf("(make new maps for network solver)\n");

     ns->grid_new->n_elems = 0;
     ns->grid_new->n_bonds = 0;

     abc_init_ints(ns->grid_new->elem_info, n_elems, 0);

     abc_init_ints(ns->map_elem_old_to_new, n_elems, -1);
     abc_init_ints(ns->map_elem_new_to_old, n_elems, -1);

     abc_init_ints(ns->map_bond_old_to_new, n_bonds, -1);
     abc_init_ints(ns->map_bond_new_to_old, n_bonds, -1);

     for (b = 0; b < n_bonds; b++)
     {
          elem1 = ns->grid_old->bond_elem1[b];
          elem2 = ns->grid_old->bond_elem2[b];

          if (not is_elem[elem1] and not is_elem[elem2]) continue;
 
          fvm_add_to_elem_map(ns, elem1);
          fvm_add_to_elem_map(ns, elem2);
          fvm_add_to_bond_map(ns, b);
     }

     fvm_copy_data_to_new_grid(ns);
}


void fvm_add_to_elem_map(
     FEMNetworkSolver *ns,
     int elem_old)
{
     int elem_new = ns->grid_new->n_elems;

     if (ns->map_elem_old_to_new[elem_old] >= 0) return;

     ns->map_elem_old_to_new[elem_old] = elem_new;
     ns->map_elem_new_to_old[elem_new] = elem_old;

     if (not ns->is_network_elem[elem_old]) 
          SetBit(ns->grid_new->elem_info[elem_new], FEMBoundaryNS);

     ns->grid_new->n_elems++;
}


void fvm_add_to_bond_map(
     FEMNetworkSolver *ns,
     int bond_old)
{
     int bond_new = ns->grid_new->n_bonds;
     int old_elem1, old_elem2, new_elem1, new_elem2;

     ns->map_bond_old_to_new[bond_old] = bond_new;
     ns->map_bond_new_to_old[bond_new] = bond_old;

     old_elem1 = ns->grid_old->bond_elem1[bond_old];
     old_elem2 = ns->grid_old->bond_elem2[bond_old];

     new_elem1 = ns->map_elem_old_to_new[old_elem1];
     new_elem2 = ns->map_elem_old_to_new[old_elem2];

     ns->grid_new->bond_elem1[bond_new] = new_elem1;
     ns->grid_new->bond_elem2[bond_new] = new_elem2;

     ns->grid_new->n_bonds++;
}


void fvm_copy_data_to_new_grid(
     FEMNetworkSolver *ns)
{
     FEMGrid *gold = ns->grid_old;
     FEMGrid *gnew = ns->grid_new;
     int dim = gold->n_dim;
     int elem_new, elem_old;
     int bond_new, bond_old;

     for (elem_new = 0; elem_new < ns->grid_new->n_elems; elem_new++)
     {
          elem_old = ns->map_elem_new_to_old[elem_new];
          gnew->volume_elem[elem_new] = gold->volume_elem[elem_old];
          gnew->xcoord_elem[elem_new] = gold->xcoord_elem[elem_old];
          if (dim > 1) gnew->ycoord_elem[elem_new] = gold->ycoord_elem[elem_old];
          if (dim > 2) gnew->zcoord_elem[elem_new] = gold->zcoord_elem[elem_old];
     }

     for (bond_new = 0; bond_new < ns->grid_new->n_bonds; bond_new++)
     {
          bond_old = ns->map_bond_new_to_old[bond_new];
          gnew->bond_area[bond_new] = gold->bond_area[bond_old];
          gnew->bond_length[bond_new] = gold->bond_length[bond_old];
     }
}


void fvm_print_network_main_bonds(
     FEMNetworkSolver *ns)
{
     int ba, bb;
     int eold1, eold2, enew1, enew2;

     printf("The MAIN-bonds:\n");

     for (ba = 0; ba < ns->grid_old->n_bonds; ba++)
     {
          bb = ns->map_bond_old_to_new[ba];

          if (bb < 0) continue;

          eold1 = ns->grid_old->bond_elem1[bb];
          eold2 = ns->grid_old->bond_elem2[bb];

          enew1 = ns->map_elem_old_to_new[eold1];
          enew2 = ns->map_elem_old_to_new[eold2];

          if (not ns->is_network_elem[eold1]) continue;
          if (not ns->is_network_elem[eold2]) continue;

          printf("MAIN-bond: %3d (%3d) e1: %3d(%3d) e2: %3d(%3d)\n",
               ba, bb, eold1, enew1, eold2, enew2);

          fvm_print_network_one_bond_old(ns, ba, "MAIN: ");
     }
}


void fvm_print_network_all_bonds(
     FEMNetworkSolver *ns)
{
     int ba;

     for (ba = 0; ba < ns->grid_new->n_bonds; ba++)
     {
          int bb = ns->map_bond_new_to_old[ba];
          fvm_print_network_one_bond_old(ns, bb, "");
     }
}


void fvm_print_network_one_bond_old(
     FEMNetworkSolver *ns,
     int ba,
     const char *text)
{
     int bb, bc; 
     int is_main, enew1a, enew2a, enew1b, enew2b, eold1b, eold2b;

     int eold1a = ns->grid_old->bond_elem1[ba];
     int eold2a = ns->grid_old->bond_elem2[ba];

     bb = ns->map_bond_old_to_new[ba];

     if (bb < 0) return;
          
     bc = ns->map_bond_new_to_old[bb];

     enew1a = ns->map_elem_old_to_new[eold1a];
     enew2a = ns->map_elem_old_to_new[eold2a];

     enew1b = ns->grid_new->bond_elem1[bb];
     enew2b = ns->grid_new->bond_elem2[bb];

     eold1b = ns->map_elem_new_to_old[enew1b];
     eold2b = ns->map_elem_new_to_old[enew2b];

     if (ns->is_network_elem[eold1b] and ns->is_network_elem[eold2b]) 
          is_main = TRUE;
     else
          is_main = FALSE;
     

     printf("%s bond: %d=%d (%d) e1: %d=%d (%d=%d) e2: %d=%d (%d=%d) %s (%d=%d)\n",
          text, ba, bc, bb, 
          eold1a, eold1b, enew1a, enew1b, 
          eold2a, eold2b, enew2a, enew2b,
          (is_main) ? "MAIN bond" : "mixed bond",
          is_main, fvm_is_main_bond(ns, ba));
}


void fvm_check_all_bonds(
     FEMNetworkSolver *ns)
{
     int b; 

     for (b = 0; b < ns->grid_old->n_bonds; b++)
          if (not fvm_is_ok_network_bond_old(ns, b))
               fvm_print_network_one_bond_old(ns, b, "ERROR: ");
}


int fvm_is_main_bond(
     FEMNetworkSolver *ns,
     int bold)
{
     int is_main_bond = FALSE;
     int eold1 = ns->grid_old->bond_elem1[bold];
     int eold2 = ns->grid_old->bond_elem2[bold];
     if ((eold1 < 0) or (eold2 < 0)) return FALSE;
     is_main_bond = (ns->is_network_elem[eold1] and ns->is_network_elem[eold2]);
     return is_main_bond;
}


int  fvm_is_ok_network_bond_old(
     FEMNetworkSolver *ns,
     int ba)
{
     int bb, bc, enew1a, enew2a, enew1b, enew2b, eold1b, eold2b;

     int errors = 0;
     int eold1a = ns->grid_old->bond_elem1[ba];
     int eold2a = ns->grid_old->bond_elem2[ba];

     bb = ns->map_bond_old_to_new[ba];

     if (bb < 0) return TRUE;

     bc = ns->map_bond_new_to_old[bb];

     enew1a = ns->map_elem_old_to_new[eold1a];
     enew2a = ns->map_elem_old_to_new[eold2a];

     enew1b = ns->grid_new->bond_elem1[bb];
     enew2b = ns->grid_new->bond_elem2[bb];

     eold1b = ns->map_elem_new_to_old[enew1b];
     eold2b = ns->map_elem_new_to_old[enew2b];

     if (ba     != bc    ) errors++;
     if (eold1a != eold1b) errors++;
     if (enew1a != enew1b) errors++;
     if (eold2a != eold2b) errors++;
     if (enew2a != enew2b) errors++;

     return (errors == 0);
}


void fvm_print_network_bonds_and_elems(
     FEMNetworkSolver *ns)
{
     int ba;
     FEMGrid *gnew = ns->grid_new;

     for (ba = 0; ba < gnew->n_bonds; ba++)
     {
          printf("bond: %3d, elem1: %3d, elem2: %3d, len=%10.3e, A=%10.3e\n",
               ba, gnew->bond_elem1[ba], gnew->bond_elem2[ba],
               gnew->bond_length[ba], gnew->bond_area[ba]);
     }
}


double fvm_get_leakage_factor(
     FEMNetworkSolver *ns)
{
     int ba;
     double sum = 0.0;
     double factor = 1.0;
     FEMGrid *gnew = ns->grid_new;

     /*
     ** The factor computes the geometrical fractor |area / length|
     ** which gives the stationary leak-off into the rock.
     ** Stationary state: |area * (k/mu) * (p / length) = Q|.
     ** It is assumed that the all damaged (or fractured) elements
     ** have the same pressure.
     ** 
     */

     for (ba = 0; ba < gnew->n_bonds; ba++)
     {
          int enew1 = ns->grid_new->bond_elem1[ba];
          int enew2 = ns->grid_new->bond_elem2[ba];

          int eold1 = ns->map_elem_new_to_old[enew1];
          int eold2 = ns->map_elem_new_to_old[enew2];

          int is1 = ns->is_network_elem[eold1];
          int is2 = ns->is_network_elem[eold2];

          if ((is1 and not is2) or (not is1 and is2))
               sum += gnew->bond_area[ba] / gnew->bond_length[ba];
     }

     if (ABC_ABS(sum) > 1.0e-9)
          factor = 1.0 / sum;
     else
          factor = 1.0;

     return factor;
}


double fvm_get_volume_factor(
     FEMNetworkSolver *ns)
{
     int enew, eold;
     double sum = 0.0;
     FEMGrid *gnew = ns->grid_new;
     FEMGrid *gold = ns->grid_old;

     for (enew = 0; enew < gnew->n_elems; enew++)
     {
          eold = ns->map_elem_new_to_old[enew];

          if (ns->is_network_elem[eold])
               sum += gold->volume_elem[eold];
     }

     return sum;
}


void fvm_show_network_solution(
     FEMNetworkSolver *ns)
{
     double *new0 = ns->pde_new->solution0->value;
     double *new1 = ns->pde_new->solution1->value;
     double *old0 = ns->pde_ptr->solution0->value;
     double *old1 = ns->pde_ptr->solution1->value;
     const char *text;
     int enew, eold;

     printf("-----The solution -----\n");

     for (eold = 0; eold < ns->grid_old->n_elems; eold++)
     {
          if (ns->is_network_elem[eold])
          {
               enew = ns->map_elem_old_to_new[eold];
               printf("MAIN: e-new: %5d, e-old: %5d, new-sol1: %7g, old-sol1: %7g\n", 
                    enew, eold, new1[enew], old1[eold]);
          }
     }

     printf("-----The solution -----\n");

     for (enew = 0; enew < ns->grid_new->n_elems; enew++)
     {
          eold = ns->map_elem_new_to_old[enew];
          text = (ns->is_network_elem[eold]) ? "MAIN" : "-";
          printf("e-new: %5d, e-old:%5d s0-new: %7g, s0-old: %7g, s1-new: %7g, s1-old: %7g, %s\n",
               enew, eold, new0[enew], old0[eold], new1[enew], old1[eold], text);

     }
}


void fvm_set_solution_to_zero(
     FEMDiffEq *pde)
{
     abc_vector_zero(pde->solution0);
     abc_vector_zero(pde->solution1);
     abc_vector_zero(pde->solution2);
}


