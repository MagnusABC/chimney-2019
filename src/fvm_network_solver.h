/*
**   This program is free software under GNU General Public License.
**   (c) Copyright 1989-2004 by Magnus Wangen
**
**   This program is free software; you can redistribute it and/or
**   modify it under the terms of the GNU General Public License as
**   published by the Free Software Foundation; either version 2 of
**   the License, or (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
**   02111-1307, USA.
**
**   $Id$
*/

#ifndef _FVM_NETWORK_SOLVER_H_
#define _FVM_NETWORK_SOLVER_H_

#define FEMBoundaryNS 2048 

typedef struct _FEMNetworkSolver_ FEMNetworkSolver;

struct _FEMNetworkSolver_ {
     int is_verbose;
     FEMDiffEq *pde_ptr;
     FEMDiffEq *pde_new;
     FEMGrid *grid_old;
     FEMGrid *grid_new;
     int *is_network_elem;
     int *map_elem_old_to_new;
     int *map_elem_new_to_old;
     int *map_bond_old_to_new;
     int *map_bond_new_to_old;
};

/* Prototypes for: fvm_network_solver.c */

void fvm_set_current_network_solver(FEMNetworkSolver *ns);
FEMNetworkSolver *fvm_get_current_network_solver(void);

FEMNetworkSolver *fvm_create_network_solver(FEMDiffEq *pde, 
     int *is_network_elem, 
     FEM_MAKE_ELEM_COEFS make_elem_coefs,
     FEM_MAKE_BOND_COEFS make_bond_coefs,
     int matrix_type);

FEMDiffEq *fvm_create_scalar_bond_pde(
     FEMGrid *grid,
     FEM_MAKE_ELEM_COEFS elem_coefs,
     FEM_MAKE_BOND_COEFS bond_coefs,
     int matrix_type);

int    fvm_network_solver_demo(int argc, char **argv);
void   fvm_delete_network_solver(FEMNetworkSolver **pp);
void   fvm_network_solve(FEMNetworkSolver *ns, double t1, double dt);
void   fvm_make_maps_for_network_solver(FEMNetworkSolver *ns);
void   fvm_add_to_elem_map(FEMNetworkSolver *ns, int elem_old);
void   fvm_add_to_bond_map(FEMNetworkSolver *ns, int bond_old);
void   fvm_copy_data_to_new_grid(FEMNetworkSolver *ns);
void   fvm_print_network_main_bonds(FEMNetworkSolver *ns);
void   fvm_print_network_all_bonds(FEMNetworkSolver *ns);
void   fvm_print_network_one_bond_old(FEMNetworkSolver *ns, int ba, const char *text);
void   fvm_check_all_bonds(FEMNetworkSolver *ns);
int    fvm_is_main_bond(FEMNetworkSolver *ns, int bold);
int    fvm_is_ok_network_bond_old(FEMNetworkSolver *ns, int ba);
void   fvm_print_network_bonds_and_elems(FEMNetworkSolver *ns);
double fvm_get_leakage_factor(FEMNetworkSolver *ns);
double fvm_get_volume_factor(FEMNetworkSolver *ns);
void   fvm_show_network_solution(FEMNetworkSolver *ns);
void   fvm_set_solution_to_zero(FEMDiffEq *pde);

#endif

