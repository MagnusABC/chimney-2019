
import lib_tex_params as lt

def make_table1():
    tab = lt.SimTexParams("x1-input.txt", "x1-table.tex")
    tab.add_param("steps",            "number of time steps",                      "-")
    tab.add_param("nn",               "number of nodes x- and y-dir",              "-")
    tab.add_param("nv",               "number of nodes z-dir",                     "-")
    tab.add_param("size",             "length x- and y-directions ($l$)",          "m")
    tab.add_param("thickness",        "thickness ($h$)",                           "m")
    tab.add_param("perm_res",         "reservoir permeability ($\\kR$)",           "\\UnitPerm")
    tab.add_param("perm_frac_open",   "open chimney permeability ($\\kO$)",        "\\UnitPerm")
    tab.add_param("perm_frac_closed", "closed chimney permeability ($\\kC$)",      "\\UnitPerm")
    tab.add_param("phi",              "damage porosity ($\\PhiD$)",                "-")
    tab.add_param("visc",             "viscosity ($\\mu$)",                        "\\UnitVisc")
    tab.add_param("compres",          "system compressibility ($\\CompD$)",        "\\UnitComp")
    tab.add_param("t_end",            "simulation time ($\\tEnd$)",                "s")
    tab.add_param("factor_x",         "factor $f_h$",                              "-")
    tab.add_param("factor_y",         "factor $f_H$",                              "-")
    tab.add_param("frac_scale",       "max bond strength ($s_0$)",                 "Pa")
    tab.is_debugging = True
    tab.make_table()

make_table1()

